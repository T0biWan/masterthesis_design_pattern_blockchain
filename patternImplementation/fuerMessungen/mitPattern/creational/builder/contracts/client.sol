pragma solidity ^0.5.0;

import "./sandwichbuilder.sol";

///@title Dient dem Testen des Builder Patterns
///@author Tobias Wamhof
contract Client {
    SandwichBuilder builder = new SandwichBuilder();

    ///@notice Testet die Erstellung eines gesunden Sandwichs
    function testeErstellungGesund() public {
        builder.erstelleGesundesSandwich();
    }

    ///@notice Testet die Erstellung eines normalen Sandwichs
    function testeErstellungNormal() public {
        builder.erstelleNormalesSandwich();
    }
}
