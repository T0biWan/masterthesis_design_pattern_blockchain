pragma solidity ^0.5.0;

import "./zutat.sol";

///@title Repräsentiert ein Sandwich, das aus verschiedenen Zutaten bestehen kann
///@author Tobias Wamhof
contract Sandwich {
    string private bezeichnung;

    Zutat[] zutaten;

    constructor(string memory _bezeichnung) public {
        bezeichnung = _bezeichnung;
    }

    ///@notice Fügt eine einzelne Zutat zum Sandwich hinzu
    ///@param _zutat di hinzuzufügende Zutat
    function zutatHinzufuegen(Zutat _zutat) public {
        zutaten.push(_zutat);
    }

    ///@notice Fügt mehrere Zutaten zum Sandwich hinzu
    ///@param _zutaten Array von hinzuzufügenden Zutaten
    function zutatenHinzufuegen(Zutat[] memory _zutaten) public {
        for (uint256 i = 0; i < _zutaten.length; i++) {
            zutaten.push(_zutaten[i]);
        }
    }

    ///@notice Liefert alle Zutaten eines Sandwichs zurück
    ///@return Array der Zutaten
    function getZutaten() public view returns (Zutat[] memory) {
        return zutaten;
    }

    ///@notice Liefert Bezeichnung des Sandwichs zurück
    ///@return Bezeichnung des Sandwichs
    function getBezeichnung() public view returns (string memory) {
        return bezeichnung;
    }

    ///@notice initialisiert die Bezeichnung eines Sandwichs
    ///@param _bezeichnung Bezeichnung des Sandwichs
    function initialize(string memory _bezeichnung) public{
        bezeichnung = _bezeichnung;
    }
}
