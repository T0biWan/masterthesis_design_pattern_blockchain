pragma solidity ^0.5.0;

import "./sandwich.sol";
import "./vollkornbrot.sol";
import "./salat.sol";
import "./weissbrot.sol";
import "./fleisch.sol";
import "./clonefactory.sol";

///@title Dient dem Erzeugen von verschiedenen Sadnwiches
///@author Tobias Wamhof
contract SandwichBuilder is CloneFactory{
    Salat salatTemplate = new Salat();
    Vollkornbrot vollkornTemplate = new Vollkornbrot();
    Fleisch fleischTemplate = new Fleisch();
    Weissbrot weissbrotTemplate = new Weissbrot();
    Sandwich sandwichTemplate = new Sandwich("Template");

    ///@notice Erstellt ein gesundes Sandwich
    function erstelleGesundesSandwich() public returns (Sandwich) {
        Sandwich sandwich = Sandwich(createClone(address(sandwichTemplate)));
        sandwich.initialize("gesund");

        Zutat[] memory zutaten = new Zutat[](2);
        Salat salat = Salat(createClone(address(salatTemplate)));
        salat.initialize("Salat");
        zutaten[0] = salat;
        Vollkornbrot vollkorn =
            Vollkornbrot(createClone(address(vollkornTemplate)));
        vollkorn.initialize("Vollkornbrot");
        zutaten[1] = vollkorn;

        sandwich.zutatenHinzufuegen(zutaten);

        return sandwich;
    }

    ///@notice Erstellt ein normales Sandwich
    function erstelleNormalesSandwich() public returns (Sandwich) {
        Sandwich sandwich = Sandwich(createClone(address(sandwichTemplate)));
        sandwich.initialize("normal");

        Zutat[] memory zutaten = new Zutat[](2);
        Fleisch fleisch = Fleisch(createClone(address(fleischTemplate)));
        fleisch.initialize("Fleisch");
        zutaten[0] = fleisch;
        Weissbrot weisbrot = Weissbrot(createClone(address(weissbrotTemplate)));
        weisbrot.initialize("Weissbrot");
        zutaten[1] = weisbrot;

        sandwich.zutatenHinzufuegen(zutaten);

        return sandwich;
    }
}
