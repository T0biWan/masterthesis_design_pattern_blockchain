pragma solidity ^0.5.0;

import "./connectionpool.sol";

///@title Dient dem Testen des Object Pool Patterns
///@author Tobias Wamhof
contract Client {
    ConnectionPool pool;

    constructor(ConnectionPool _pool) public {
        pool = _pool;
    }

    ///@notice Testet Anfordern und Freigeben einer Verbindung
    function t1() public {
        Connection con;
        con = pool.fordereVerbindung();
        pool.gebeFrei(con);
    }

    ///@notice Testet Anfordern und Freigeben zweier Verbindungen
    function t3() public {
        Connection con;
        Connection con2;
        con = pool.fordereVerbindung();
        con2 = pool.fordereVerbindung();
        pool.gebeFrei(con2);
        pool.gebeFrei(con);
    }
}
