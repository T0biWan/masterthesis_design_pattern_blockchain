pragma solidity ^0.5.0;

import "./connection.sol";

///@title Verwaltet eine Liste von Connection Instanzen
///@author Tobias Wamhof
contract ConnectionPool {
    Connection[] verbindungen;
    mapping(uint256 => bool) verbindungenCheck;
    uint256 frei;
    uint256 gesamtZahl;

    constructor(uint256 _zuErzeugendeVerbindungen) public {
        instanziiere(_zuErzeugendeVerbindungen);
    }

    ///@notice Liefert eine freie Verbindung zurück, falls eine existiert
    ///@return eine freie Verbindung
    function fordereVerbindung() public returns (Connection) {
        require(frei > 0);
        for (uint256 i = 0; i < verbindungen.length; i++) {
            if (verbindungenCheck[i]) {
                verbindungenCheck[i] = false;
                frei--;
                return verbindungen[i];
            }
        }
    }

    ///@notice Gibt eine Verbindung frei, die zuvor genutzt wurde
    ///@param _connection die genutzte Verbindung
    function gebeFrei(Connection _connection) public {
        require(frei < gesamtZahl);
        for (uint256 i = 0; i < verbindungen.length; i++) {
            if ( address(_connection) == address(verbindungen[i])            ) {
                verbindungenCheck[i] = true;
                frei++;
                break;
            }
        }
    }

    ///@notice Setzt Anzahl der zu erzeugenden Instanzen und instanziiert diese
    ///@param _anzahl Anzahl zu erzeugender Instanzen
    function setZuErzeugendeInstanzen(uint256 _anzahl) public {
        instanziiere(_anzahl);
    }

    ///@notice Instantiiert die übergebe Anzahl an Verbindungen
    ///@param _zuErzeugendeObjekte Anzahl der benötigten Instanzen
    function instanziiere(uint256 _zuErzeugendeObjekte) private {
        for (uint256 i = 0; i < gesamtZahl; i++) {
            verbindungenCheck[i] = false;
        }

        gesamtZahl = _zuErzeugendeObjekte;

        delete verbindungen;
        frei = _zuErzeugendeObjekte;
        gesamtZahl = _zuErzeugendeObjekte;

        for (uint256 i = 0; i < _zuErzeugendeObjekte; i++) {
            Connection con = new Connection("Host", i);
            verbindungen.push(con);
            verbindungenCheck[i] = true;
        }
    }

    ///@notice Liefert die Anzahl freier Verbindungen
    ///@return Anzahl freier Verbindungen
    function getAnzahlFreierVerbindungen() public view returns(uint){
        return frei;
    }
}
