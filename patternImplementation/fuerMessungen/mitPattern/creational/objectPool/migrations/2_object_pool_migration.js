const Connection = artifacts.require("Connection");
const ConnectionPool = artifacts.require("ConnectionPool");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Connection, "Test", -1);
  deployer.deploy(ConnectionPool, 10).then(function () {
    return deployer.deploy(Client, ConnectionPool.address);
  })
};
