pragma solidity ^0.5.0;

import "./geometrischefigurfactory.sol";
import "./dreieckfactory.sol";
import "./rechteckfactory.sol";

///@title Dient dem Testen des Factory (Method) Patterns
///@author Tobias Wamhof
contract Client {
    GeometrischeFigurFactory factory;

    ///@notice Testet Generierung eines Dreickecks
    function testeDreieckGenerierung() public {
        factory = new DreieckFactory();
        GeometrischeFigur ergebnis = factory.erzeugeInstanz();
    }

    ///@notice Testet Generierung eines Rechtecks
    function testeRechteckGenerierung() public {
        factory = new RechteckFactory();
        GeometrischeFigur ergebnis = factory.erzeugeInstanz();
    }
}
