pragma solidity ^0.5.0;

import "./geometrischefigur.sol";
import "./clonefactory.sol";

///@title Gibt Struktur einer Fabrik zur Erzeugung von geometrischen Figuren vor
///@author Tobias Wamhof
contract GeometrischeFigurFactory is CloneFactory {

    ///@notice Erzeugt eine Instanz der jeweils zu erzeugenden geometrischen Figur
    ///@return die erzeugte Instanz
    function erzeugeInstanz() public returns (GeometrischeFigur);
}
