pragma solidity ^0.5.0;

import "./geometrischefigur.sol";

///@title Repräsentation eines Dreiecks
///@author Tobias Wamhof
contract Dreieck is GeometrischeFigur {

    ///@notice Gibt den Figurentyp zurück
    ///@return der Figurentyp
    function getFigurTyp() external pure returns (string memory) {
        return "Dreieck";
    }
}
