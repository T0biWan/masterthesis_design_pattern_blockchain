pragma solidity ^0.5.0;

import "./geometrischefigurfactory.sol";

///@title Dient dem Testen des Factory (Method) Patterns
///@author Tobias Wamhof
contract Client {
    GeometrischeFigurFactory factory = new GeometrischeFigurFactory();

    ///@notice Testet Generierung eines Dreickecks
    function testeDreieckGenerierung() public {
        GeometrischeFigur ergebnis = factory.erzeugeInstanz(2);
    }

    ///@notice Testet Generierung eines Rechtecks
    function testeRechteckGenerierung() public {
        GeometrischeFigur ergebnis = factory.erzeugeInstanz(1);
    }
}
