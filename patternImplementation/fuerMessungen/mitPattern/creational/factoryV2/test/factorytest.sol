pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/geometrischefigur.sol";
import "../contracts/geometrischefigurfactory.sol";


///@title Dient dem Testen des Factory (Method) Patterns
///@author Tobias Wamhof
contract FactoryTest{

    string dreieckTest = "Dreieck";
    string rechteckTest = "Rechteck";

    GeometrischeFigurFactory factory = GeometrischeFigurFactory(DeployedAddresses.GeometrischeFigurFactory());
    GeometrischeFigur result;

    ///@notice Testet Generierung eines Dreickecks
    function testeErstellungDreieck() public{
        result = factory.erzeugeInstanz(2);

        Assert.equal(result.getFigurTyp(), dreieckTest, "Should be the same");
    }

    ///@notice Testet Generierung eines Rechtecks
    function testeErstellungRechteck() public{
        result = factory.erzeugeInstanz(1);

        Assert.equal(result.getFigurTyp(), rechteckTest, "Should be the same");
    }

    ///@notice Testet ungültige Eingabe
    function testeUngueltigeEingabe() public {
        bytes memory payload = abi.encodeWithSignature("erzeugeInstanz()", 3);
        (bool success, bytes memory response) = address(factory).call(payload);

        Assert.isFalse(success, "Should not work with invalid index");
    }

}