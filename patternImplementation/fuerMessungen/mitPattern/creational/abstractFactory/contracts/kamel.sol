pragma solidity ^0.5.0;

import "./tier.sol";

///@title Spezielles Tier der Wüste
///@author Tobias Wamhof
contract Kamel is Tier {

    ///@notice Gibt eine Beschreibung der erzeugten Instanz aus
    ///@return die Beschreibung
    function ausgabe() external pure returns (string memory) {
        return "Ich bin ein Kamel.";
    }
}
