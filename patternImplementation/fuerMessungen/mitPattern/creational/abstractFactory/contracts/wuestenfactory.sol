pragma solidity ^0.5.0;

import "./abstraktterrainfactory.sol";
import "./untergrund.sol";
import "./tier.sol";
import "./sand.sol";
import "./kamel.sol";
import "./clonefactory.sol";

///@title Dient dem Erzeugen von Instanzen von Contracts, die zur Wüste gehören
///@author Tobias Wamhof
contract WuestenFactory is AbstraktTerrainFactory, CloneFactory {
    Sand sandTemplate = new Sand();
    Kamel kamelTemplate = new Kamel();

    ///@notice Erzeugt einen Wüstenspezifischen Untergrund
    ///@return spezifischer Untergrund
    function erstelleUntergrund() external returns (Untergrund) {
        return Sand(createClone(address(sandTemplate)));
    }

    ///@notice Erzeugt ein Wüstenspezifisches Tier
    ///@return spezifisches Tier
    function erstelleTier() external returns (Tier) {
        return Kamel(createClone(address(kamelTemplate)));
    }
}
