pragma solidity ^0.5.0;

import "./untergrund.sol";

///@title Spezieller Untergrund des Regenwalds
///@author Tobias Wamhof
contract Gras is Untergrund {

    ///@notice Gibt eine Beschreibung der erzeugten Instanz aus
    ///@return die Beschreibung
    function ausgabe() external pure returns (string memory) {
        return "Der Untergrund besteht aus Gras.";
    }
}
