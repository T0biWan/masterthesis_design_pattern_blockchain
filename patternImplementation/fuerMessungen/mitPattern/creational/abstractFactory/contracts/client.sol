pragma solidity ^0.5.0;

import "./abstraktterrainfactory.sol";
import "./regenwaldfactory.sol";
import "./wuestenfactory.sol";

///@title Dient dem Testen des Abstract Factory Patterns
///@author Tobias Wamhof
contract Client {
    AbstraktTerrainFactory factory;

    ///@notice Testet Erzeugen von Objekten der Regenwaldfabrik
    function testeRegenwaldFactory() public {
        factory = new RegenwaldFactory();
        Untergrund untergrund = factory.erstelleUntergrund();
        Tier tier = factory.erstelleTier();
    }

    ///@notice Testet Erzeugen von Objekten der Wüstenfabrik
    function testeWuestenFactory() public {
        factory = new WuestenFactory();
        Untergrund untergrund = factory.erstelleUntergrund();
        Tier tier = factory.erstelleTier();
    }
}
