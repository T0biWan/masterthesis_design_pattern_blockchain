pragma solidity ^0.5.0;

import "./clonefactory.sol";
import "./arbeit.sol";
import "./urlaub.sol";
import "./dienstplanobjekt.sol";

///@title Dient dem Klonen von verschiedenen Dienstplanobjekten
///@author Tobias Wamhof
contract DienstplanObjektCloner is CloneFactory{
    Arbeit arbeitTemplate = new Arbeit(1);
    Urlaub urlaubTemplate = new Urlaub(1);

    ///@notice Klont ein DienstplanObjekt vom Typ Arbeit
    ///@param _monat Monat der nach dem Klonen gesetzt werden soll
    ///@return die erzeugte Instanz
    function erzeugeArbeitsObjekt(uint _monat)public returns(DienstplanObjekt) {
        Arbeit result = Arbeit(createClone(address(arbeitTemplate)));
        result.initialize(_monat);
        return result;
    }
    
    ///@notice Klont ein DienstplanObjekt vom Typ Urlaub
    ///@param _monat Monat der nach dem Klonen gesetzt werden soll
    ///@return die erzeugte Instanz
    function erzeugeUrlaubsObjekt(uint _monat)public returns (DienstplanObjekt) {
        Urlaub result = Urlaub(createClone(address(urlaubTemplate)));
        result.initialize(_monat);
        return result;
    }
}