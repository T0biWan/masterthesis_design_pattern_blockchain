pragma solidity ^0.5.0;

///@title Leere Contract für Kostenmessungen
///@author Tobias Wamhof
contract Client{

    ///@notice Leere Methode
    ///@return 1
    function someFunction() public returns(uint){
        return 1;
    }

}
