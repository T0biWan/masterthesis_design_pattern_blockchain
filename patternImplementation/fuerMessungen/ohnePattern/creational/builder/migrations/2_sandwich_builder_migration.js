const Weissbrot = artifacts.require("Weissbrot");
const Vollkornbrot = artifacts.require("Vollkornbrot");
const Salat = artifacts.require("Salat");
const Fleisch = artifacts.require("Fleisch");
const Sandwich = artifacts.require("Sandwich");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Weissbrot);
  deployer.deploy(Vollkornbrot);
  deployer.deploy(Salat);
  deployer.deploy(Fleisch);
  deployer.deploy(Sandwich, "first");
  deployer.deploy(Client);
};
