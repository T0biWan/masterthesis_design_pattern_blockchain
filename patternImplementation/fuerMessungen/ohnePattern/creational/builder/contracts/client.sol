pragma solidity ^0.5.0;

import "./sandwich.sol";
import "./vollkornbrot.sol";
import "./salat.sol";
import "./weissbrot.sol";
import "./fleisch.sol";
import "./clonefactory.sol";

contract Client is CloneFactory{
    Salat salatTemplate = new Salat();
    Vollkornbrot vollkornTemplate = new Vollkornbrot();
    Fleisch fleischTemplate = new Fleisch();
    Weissbrot weissbrotTemplate = new Weissbrot();
    Sandwich sandwichTemplate = new Sandwich("Template");

    function testeErstellungGesund() public {
        Sandwich sandwich = Sandwich(createClone(address(sandwichTemplate)));
        sandwich.initialize("gesund");

        Zutat[] memory zutaten = new Zutat[](2);
        zutaten[0] = Salat(createClone(address(salatTemplate)));
        zutaten[1] = Vollkornbrot(createClone(address(vollkornTemplate)));

        sandwich.zutatenHinzufuegen(zutaten);
    }

    function testeErstellungNormal() public {
        Sandwich sandwich = Sandwich(createClone(address(sandwichTemplate)));
        sandwich.initialize("normal");

        Zutat[] memory zutaten = new Zutat[](2);
        zutaten[0] = Fleisch(createClone(address(fleischTemplate)));
        zutaten[1] = Weissbrot(createClone(address(weissbrotTemplate)));

        sandwich.zutatenHinzufuegen(zutaten);
    }
}
