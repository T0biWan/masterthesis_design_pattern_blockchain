pragma solidity ^0.5.0;

import "./zutat.sol";

contract Sandwich {
    string private bezeichnung;

    Zutat[] zutaten;

    constructor(string memory _bezeichnung) public {
        bezeichnung = _bezeichnung;
    }

    function zutatHinzufuegen(Zutat _zutat) public {
        zutaten.push(_zutat);
    }

    function zutatenHinzufuegen(Zutat[] memory _zutaten) public {
        for (uint256 i = 0; i < _zutaten.length; i++) {
            zutaten.push(_zutaten[i]);
        }
    }

    function getZutaten() public view returns (Zutat[] memory) {
        return zutaten;
    }

    function getBezeichnung() public view returns (string memory) {
        return bezeichnung;
    }

    function initialize(string memory _bezeichnung) public{
        bezeichnung = _bezeichnung;
    }
}
