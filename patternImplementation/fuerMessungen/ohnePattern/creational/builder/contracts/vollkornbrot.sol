pragma solidity ^0.5.0;

import "./brot.sol";

contract Vollkornbrot is Brot {
    constructor() public {
        bezeichnung = "Vollkornbrot";
    }
}
