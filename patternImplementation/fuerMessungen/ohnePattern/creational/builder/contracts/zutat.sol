pragma solidity ^0.5.0;

contract Zutat {
    string internal bezeichnung;

    function getBezeichnung() public view returns (string memory) {
        return bezeichnung;
    }
}
