const Dreieck = artifacts.require("Dreieck");
const Rechteck = artifacts.require("Rechteck");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Dreieck);
  deployer.deploy(Rechteck);
  deployer.deploy(Client);
};
