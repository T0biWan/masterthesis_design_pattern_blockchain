pragma solidity ^0.5.0;

import "./dreieck.sol";
import "./rechteck.sol";
import "./clonefactory.sol";

///@title Dient dem Testen des Factory (Method) Patterns
///@author Tobias Wamhof
contract Client is CloneFactory{
    Dreieck dreieck = new Dreieck();
    Rechteck viereck = new Rechteck();

    ///@notice Testet Generierung eines Dreickecks
    function testeDreieckGenerierung() public {
        GeometrischeFigur ergebnis = Dreieck(createClone(address(dreieck)));
    }

    ///@notice Testet Generierung eines Rechtecks
    function testeRechteckGenerierung() public {
        GeometrischeFigur ergebnis = Rechteck(createClone(address(viereck)));
    }
}
