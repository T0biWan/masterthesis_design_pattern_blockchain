pragma solidity ^0.5.0;

///@title Repräsentiert eine Geometrische Figur
///@author Tobias Wamhof
interface GeometrischeFigur {

    ///@notice Liefert konkreten Typ der Geometrischen Figur zurück
    ///@return konkreter Typ der geometrischen Figur
    function getFigurTyp() external pure returns (string memory);
}
