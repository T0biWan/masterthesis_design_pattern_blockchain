const Connection = artifacts.require("Connection");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Connection, "Test", -1);
  deployer.deploy(Client);
};
