pragma solidity ^0.5.0;

import "./clonefactory.sol";
import "./connection.sol";

///@title Dient dem Testen des Object Pool Patterns
///@author Tobias Wamhof
contract Client is CloneFactory{
    Connection template = new Connection("", 1234);

    ///@notice Testet Anfordern und Freigeben einer Verbindung
    function t1() public {
        Connection con = Connection(createClone(address(template)));
    }

    ///@notice Testet Anfordern und Freigeben zweier Verbindungen   
    function t3() public {
        Connection con = Connection(createClone(address(template)));
        Connection con2 = Connection(createClone(address(template)));
    }
}
