pragma solidity ^0.5.0;

import "./arbeit.sol";
import "./urlaub.sol";
import "./dienstplanobjekt.sol";

///@title Dient dem Erzeugen von verschiedenen Dienstplanobjekten
///@author Tobias Wamhof
contract DienstplanObjektCloner{

    ///@notice Erzeugt ein DienstplanObjekt vom Typ Arbeit
    ///@param _monat Monat der nach dem Erzeugen gesetzt werden soll
    ///@return die erzeugte Instanz
    function erzeugeArbeitsObjekt(uint _monat)public returns(DienstplanObjekt) {
        Arbeit result = new Arbeit(_monat);
        return result;
    }
    
    ///@notice Erzeugt ein DienstplanObjekt vom Typ Urlaub
    ///@param _monat Monat der nach dem Erzeugen gesetzt werden soll
    ///@return die erzeugte Instanz
    function erzeugeUrlaubsObjekt(uint _monat)public returns (DienstplanObjekt) {
        Urlaub result = Urlaub(_monat);
        return result;
    }
}