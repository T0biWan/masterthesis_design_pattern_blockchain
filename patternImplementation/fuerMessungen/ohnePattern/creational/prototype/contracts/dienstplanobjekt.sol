pragma solidity ^0.5.0;

///@title Ein zu klonendes Dienstplanobjekt
///@author Tobias Wamhof
contract DienstplanObjekt{
    uint monat;

    constructor (uint _monat)public {
        require(_monat > 0 && monat < 13);
        monat = _monat;
    }

    ///@notice Liefert betrachteten Monat
    ///@return Monat des Dienstplanobjektes
    function getMonat() public view returns(uint){
        return monat;
    }

    ///@notice Setzt den Monat des Dienstplanobjektes
    ///@param _monat Monat des Objektes
    function setMonat(uint _monat) public{
        monat = _monat;
    }

    ///@notice Liefert den Typ des Dienstplanobjektes zurück
    ///@return Typ des Dienstplanobjektes
    function getTyp() public pure returns(string memory);
}