pragma solidity ^0.5.0;

import "./dienstplanobjektcloner.sol";
import "./dienstplanobjekt.sol";

///@title Dient dem Testen des Prototype Patterns
///@author Tobias Wamhof
contract Client{
    DienstplanObjektCloner cloner = new DienstplanObjektCloner();

    ///@notice Testet die Generierung und Editierung von Klonen
    function testGenerierung() public {
        DienstplanObjekt arbeit1 = cloner.erzeugeArbeitsObjekt(1);
        DienstplanObjekt urlaub2 = cloner.erzeugeUrlaubsObjekt(2);
        DienstplanObjekt arbeit3 = cloner.erzeugeArbeitsObjekt(3);

        arbeit1.setMonat(4);
    }
}