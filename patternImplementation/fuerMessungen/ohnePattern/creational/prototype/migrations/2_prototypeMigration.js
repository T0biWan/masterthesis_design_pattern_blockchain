const Arbeit = artifacts.require("Arbeit");
const Urlaub = artifacts.require("Urlaub");
const DienstplanObjektCloner = artifacts.require("DienstplanObjektCloner");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Arbeit, 1);
  deployer.deploy(Urlaub, 1);
  deployer.deploy(DienstplanObjektCloner);
  deployer.deploy(Client);
};
