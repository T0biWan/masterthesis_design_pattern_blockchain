pragma solidity ^0.5.0;

///@title Interface für verschiedene zu erzeugende Tiere
///@author Tobias Wamhof
interface Tier {

    ///@notice Gibt nähere Informationen zum Tier aus
    ///@return Beschreibung des Tieres
    function ausgabe() external pure returns (string memory);
}
