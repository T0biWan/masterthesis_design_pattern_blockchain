pragma solidity ^0.5.0;

import "./untergrund.sol";
import "./tier.sol";
import "./gras.sol";
import "./papagei.sol";
import "./clonefactory.sol";
import "./sand.sol";
import "./kamel.sol";

///@title Dient dem Testen des Abstract Factory Patterns
///@author Tobias Wamhof
contract Client is CloneFactory{
    uint terrainType=0;

    Gras grasTemplate = new Gras();
    Papagei papageiTemplate = new Papagei();
    Sand sandTemplate = new Sand();
    Kamel kamelTemplate = new Kamel();

    ///@notice Testet Erzeugen von Objekten verschiedenen Terrains
    function erzeugeTerrain() public {
        Untergrund untergrund;
        Tier tier;
        if(terrainType == 0){
            untergrund = Gras(createClone(address(grasTemplate)));
            tier = Papagei(createClone(address(papageiTemplate)));
        }else if(terrainType == 1){
            untergrund = Sand(createClone(address(sandTemplate)));
            tier = Kamel(createClone(address(kamelTemplate)));
        }
    }

    ///@notice Ändert das Terrain
    ///@param _terrain neues Terrain
    function changeTerrain(uint _terrain) public {
        terrainType=_terrain;
    }
}
