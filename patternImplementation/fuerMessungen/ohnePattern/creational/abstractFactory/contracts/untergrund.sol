pragma solidity ^0.5.0;

///@title Interface für verschiedene zu erzeugende Untergründe
///@author Tobias Wamhof
interface Untergrund {

    ///@notice Gibt nähere Informationen zum Untergrund aus
    ///@return Beschreibung des Untergrunds
    function ausgabe() external pure returns (string memory);
}
