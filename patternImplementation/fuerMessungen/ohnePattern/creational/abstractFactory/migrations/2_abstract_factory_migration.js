const Gras = artifacts.require("Gras");
const Sand = artifacts.require("Sand");
const Kamel = artifacts.require("Kamel");
const Papagei = artifacts.require("Papagei");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Gras);
  deployer.deploy(Sand);
  deployer.deploy(Kamel);
  deployer.deploy(Papagei);
  deployer.deploy(Client);
};
