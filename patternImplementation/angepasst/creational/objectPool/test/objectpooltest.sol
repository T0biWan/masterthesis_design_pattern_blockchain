pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/connectionpool.sol";
import "../contracts/connection.sol";

///@title Dient dem Testen des Object Pool Patterns
///@author Tobias Wamhof
contract ObjectPoolTest{

    ConnectionPool pool = ConnectionPool(DeployedAddresses.ConnectionPool());

    ///@notice Testet das Anfordern und Freigeben einer Verbindung 
    function testeAnfordernUndFreigebenEinerVerbindung() public{
        pool.setZuErzeugendeInstanzen(1);
        uint frei = pool.getAnzahlFreierVerbindungen();

        Assert.equal(frei, 1, "One Connection should be available.");

        Connection connection = pool.fordereVerbindung();

        Assert.equal(connection.getPort(), 0, "Should be the same");
        Assert.equal(frei - 1, 0, "No Connection should be available.");

        pool.gebeFrei(connection);

        Assert.equal(frei, 1, "One Connection should be available.");
    }

    ///@notice Testet das Anfordern und Freigeben von zwei Verbindungen
    function testeAnfordernUndFreigebenVonZweiVerbindungen() public{
        pool.setZuErzeugendeInstanzen(2);
        uint frei = pool.getAnzahlFreierVerbindungen();

        Assert.equal(frei, 2, "Two Connections should be available.");

        Connection connection = pool.fordereVerbindung();
        Assert.equal(connection.getPort(), 0, "Should be the same");
        Assert.equal(frei - 1, 1, "One Connection should be available.");

        pool.gebeFrei(connection);

        Assert.equal(frei, 2, "Two Connections should be available.");

        connection = pool.fordereVerbindung();

        Assert.equal(frei - 1, 1, "One Connection should be available.");
        Assert.equal(connection.getPort(), 0, "Should be the same");

        pool.gebeFrei(connection);
        Assert.equal(frei, 2, "Two Connections should be available.");
    }

    ///@notice Testet ungültige EIngabe
    function testeUngueltigeEingabe() public {
        pool.setZuErzeugendeInstanzen(1);
        Connection connection = pool.fordereVerbindung();

        bytes memory payload = abi.encodeWithSignature("fordereVerbindung()");
        (bool success, bytes memory response) = address(pool).call(payload);

        Assert.equal(connection.getPort(), 0, "Should be the same.");
        Assert.isFalse(success, "Should not work with invalid index");
    }

}