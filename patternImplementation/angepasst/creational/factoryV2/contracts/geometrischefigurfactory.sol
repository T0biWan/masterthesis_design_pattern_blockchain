pragma solidity ^0.5.0;

import "./geometrischefigur.sol";
import "./rechteck.sol";
import "./dreieck.sol";
import "./clonefactory.sol";

///@title Erzeugt verschiedene geometrische Figuren
///@author Tobias Wamhof
contract GeometrischeFigurFactory is CloneFactory {
    Dreieck templateD = new Dreieck();
    Rechteck templateR = new Rechteck();

    address[] figuren;

    ///@notice Erzeugt je nach übergebenem Parameter eine Implementierung der geometrischen Figur
    ///@param _type typ der zu erzeugenden geometrischen Figur (1=Rechteck, 2=Dreieck)
    ///@return die erzeugte Instanz
    function erzeugeInstanz(uint256 _type) public returns (GeometrischeFigur) {
        require(_type >= 1 && _type < 3);
        if (_type == 1) {
            return Rechteck(createClone(address(templateR)));
        } else if (_type == 2) {
            return Dreieck(createClone(address(templateD)));
        }
    }

    ///@notice Erzeugt je nach übergebenem Parameter eine Implementierung der geometrischen Figur
    ///@param _type typ der zu erzeugenden geometrischen Figur
    ///@return die erzeugte Instanz
    function erzeugeInstanzErweiterbar(uint256 _type) public returns (GeometrischeFigur) {
        require(_type >= 0 && _type < figuren.length);
        return GeometrischeFigur(createClone(figuren[_type]));
    }

    ///@notice Fügt eine zu erzeugende Figur hinzu
    ///@param _figur Adresse des Contracts
    function figurHinzufuegen(address _figur) public{
        figuren.push(_figur);
    }

    ///@notice Leert Liste zu erzeugender Figuren
    function clean() public {
        delete figuren;
        figuren.length = 0;
    }
}
