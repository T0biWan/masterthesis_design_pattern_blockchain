const Dreieck = artifacts.require("Dreieck");
const Rechteck = artifacts.require("Rechteck");
const CloneFactory = artifacts.require("CloneFactory");
const GeometrischeFigurFactory = artifacts.require("GeometrischeFigurFactory");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Dreieck);
  deployer.deploy(Rechteck);
  deployer.deploy(CloneFactory);
  deployer.deploy(GeometrischeFigurFactory);
  deployer.deploy(Client);
};
