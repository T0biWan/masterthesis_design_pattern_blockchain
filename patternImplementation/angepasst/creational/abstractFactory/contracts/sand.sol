pragma solidity ^0.5.0;

import "./untergrund.sol";

///@title Spezieller Untergrund der Wüste
///@author Tobias Wamhof
contract Sand is Untergrund {
    
    ///@notice Gibt eine Beschreibung der erzeugten Instanz aus
    ///@return die Beschreibung
    function ausgabe() external pure returns (string memory) {
        return "Der Untergrund besteht aus Sand.";
    }
}
