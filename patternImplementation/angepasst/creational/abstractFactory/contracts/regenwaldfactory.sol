pragma solidity ^0.5.0;

import "./abstraktterrainfactory.sol";
import "./untergrund.sol";
import "./tier.sol";
import "./gras.sol";
import "./papagei.sol";
import "./clonefactory.sol";

///@title Dient dem Erzeugen von Instanzen von Contracts, die zum Regenwald gehören
///@author Tobias Wamhof
contract RegenwaldFactory is AbstraktTerrainFactory, CloneFactory {
    Gras grasTemplate = new Gras();
    Papagei papageiTemplate = new Papagei();

    ///@notice Erzeugt einen Regenwaldspezifischen Untergrund
    ///@return spezifischer Untergrund
    function erstelleUntergrund() external returns (Untergrund) {
        return Gras(createClone(address(grasTemplate)));
    }

    ///@notice Erzeugt ein Regenwaldspezifisches Tier
    ///@return spezifisches Tier
    function erstelleTier() external returns (Tier) {
        return Papagei(createClone(address(papageiTemplate)));
    }
}
