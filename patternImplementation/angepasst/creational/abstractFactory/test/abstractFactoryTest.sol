pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/abstraktterrainfactory.sol";
import "../contracts/regenwaldfactory.sol";
import "../contracts/wuestenfactory.sol";

///@title Dient dem Testen des Abstract Factory Patterns
///@author Tobias Wamhof
contract AbstractFactoryTest{

    string papageiTest = "Ich bin ein Papagei.";
    string kamelTest = "Ich bin ein Kamel.";
    string grasTest = "Der Untergrund besteht aus Gras.";
    string sandTest = "Der Untergrund besteht aus Sand.";

    AbstraktTerrainFactory factory;

    ///@notice Testet das Erstellen des Untergrundes im Regenwald
    function testeUntergrundErstellungRegenwald() public{
        factory = RegenwaldFactory(DeployedAddresses.RegenwaldFactory());

        Assert.equal(factory.erstelleUntergrund().ausgabe(), grasTest, "Should be the same.");
    }

    ///@notice Testet das Erstellen des Untergrundes in der Wüste
    function testeUntergrundErstellungWueste() public {
        factory = WuestenFactory(DeployedAddresses.WuestenFactory());

        Assert.equal(factory.erstelleUntergrund().ausgabe(), sandTest, "Should be the same.");
    }

    ///@notice Testet das Erstellen des Tieres im Regenwald
    function testeTierErstellungRegenwald() public {
        factory = RegenwaldFactory(DeployedAddresses.RegenwaldFactory());

        Assert.equal(factory.erstelleTier().ausgabe(), papageiTest, "Should be the same.");
    }

    ///@notice Testet das Erstellen des Tieres in der Wüste
    function testeTierErstellungWueste() public {
        factory = WuestenFactory(DeployedAddresses.WuestenFactory());

        Assert.equal(factory.erstelleTier().ausgabe(), kamelTest, "Should be the same.");
    }

}