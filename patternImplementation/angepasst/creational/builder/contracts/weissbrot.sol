pragma solidity ^0.5.0;

import "./brot.sol";

///@title Spezielle From des Brotes
///@author Tobias Wamhof
contract Weissbrot is Brot {
    function initialize() public{
        bezeichnung = "Weissbrot";
    }
}
