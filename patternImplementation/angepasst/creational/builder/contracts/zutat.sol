pragma solidity ^0.5.0;

///@title Repräsentiert Zutaten, die in einem Sandwich enthalten sein können
///@author Tobias Wamhof
contract Zutat {
    string internal bezeichnung;

    ///@notice Liefert eine Beschreibung der jeweiligen Zutat
    ///@return Bezeichnung der Zutat
    function getBezeichnung() public view returns (string memory) {
        return bezeichnung;
    }

    function initialize() public;
}
