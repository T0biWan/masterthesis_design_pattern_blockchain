pragma solidity ^0.5.0;

import "./belag.sol";

///@title Spezielle From des Belags
///@author Tobias Wamhof
contract Salat is Belag {
    function initialize() public{
        bezeichnung = "Salat";
    }
}
