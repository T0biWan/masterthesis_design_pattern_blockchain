pragma solidity ^0.5.0;

contract Beispiel {
    string bezeichnung;

    function getBezeichnung() public view returns (string memory) {
        return bezeichnung;
    }

    function initialize() public{
        bezeichnung = "Beispiel";
    }
}