pragma solidity ^0.5.0;

import "./sandwich.sol";
import "./clonefactory.sol";

///@title Dient dem Erzeugen von verschiedenen Sadnwiches
///@author Tobias Wamhof
contract SandwichBuilder is CloneFactory {
    address[] zutaten;
    mapping(string => uint[]) sandwiches;
    string[] keys;
    Sandwich sandwichTemplate = new Sandwich("Template");

    ///@notice Erstellt ein Sandwich, abhängig von der übergebenen Bezeichnung
    ///@return Instanziiertes Sandwich
    function erstelleSandwich(string memory _sandwich) public returns (Sandwich) {
        Sandwich sandwich = Sandwich(createClone(address(sandwichTemplate)));
        sandwich.initialize(_sandwich);

        for(uint i = 0; i < sandwiches[_sandwich].length; i++){
            Zutat neu = Zutat(zutaten[sandwiches[_sandwich][i]]);
            neu.initialize();
            sandwich.zutatHinzufuegen(neu);
        }
        return sandwich;
    }

    ///@notice Fügt eine Zutat zur Liste aller verfügbaren Zutaten hinzu
    ///@param _zutat hinzuzufügende Zutat
    function zutatHinzufuegen(address _zutat) public{
        zutaten.push(_zutat);
    }

    ///@notice Fügt eine Zutat zu den Zutaten eines Sandwichs hinzu
    ///@param _sandwich zu ergänzendes Sandwich
    ///@param _zutat hinzuzufügende Zutat
    function zutatZuSandwichhinzufuegen(string memory _sandwich, uint _zutat) public{
        if(sandwiches[_sandwich].length == 0){
            keys.push(_sandwich);
        }
        sandwiches[_sandwich].push(_zutat);
    }

    ///@notice Löscht alle Sandwichs und Zutaten
    function clean() public {
        delete zutaten;
        for(uint i = 0; i < keys.length; i++){
            delete sandwiches[keys[i]];
        }
        delete keys;
    }
}
