pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/sandwichbuilder.sol";
import "../contracts/sandwich.sol";
import "../contracts/beispieldurcherweiterung.sol";

///@title Dient dem Testen ders Builder Patterns
///@author Tobias Wamhof
contract BuilderTest{

    Zutat fleisch = Zutat(DeployedAddresses.Fleisch());
    Zutat salat = Zutat(DeployedAddresses.Salat());
    Zutat vollkorn = Zutat(DeployedAddresses.Vollkornbrot());
    Zutat weiss = Zutat(DeployedAddresses.Weissbrot());
    Beispiel beispiel = Beispiel(DeployedAddresses.Beispiel());

    SandwichBuilder builder = SandwichBuilder(DeployedAddresses.SandwichBuilder());
    Sandwich result;

    ///@notice Testet die Generierung eines gesunden Sandwichs
    function testeErstellungGesundesSandwich() public{
        initial();

        result = builder.erstelleSandwich("gesund");

        Assert.equal(result.getZutaten()[0].getBezeichnung(), "Salat", "Should be the same");
        Assert.equal(result.getZutaten()[1].getBezeichnung(), "Vollkornbrot", "Should be the same");
    }

    ///@notice Testet die Generierung eines normalen Sandwichs
    function testeErstellungNormalesSandwich() public{
        initial();

        result = builder.erstelleSandwich("normal");

        Assert.equal(result.getZutaten()[0].getBezeichnung(), "Fleisch", "Should be the same");
        Assert.equal(result.getZutaten()[1].getBezeichnung(), "Weissbrot", "Should be the same");
        Assert.equal(result.getZutaten()[2].getBezeichnung(), "Beispiel", "Should be the same");
    }

    ///@notice Testet die Generierung eines invaliden Sandwichs
    function testeErstellungInvalidesSandwich() public{
        initial();

        result = builder.erstelleSandwich("ungueltig");

        Assert.equal(result.getZutaten().length, 0, "Should be the same");
    }

    ///@notice Initialisiert die benötigten Zutaten
    function initial() private{
        builder.clean();
        fleisch.initialize();
        salat.initialize();
        vollkorn.initialize();
        weiss.initialize();
        beispiel.initialize();

        builder.zutatHinzufuegen(address(fleisch));
        builder.zutatHinzufuegen(address(salat));
        builder.zutatHinzufuegen(address(vollkorn));
        builder.zutatHinzufuegen(address(weiss));
        builder.zutatHinzufuegen(address(beispiel));
        builder.zutatZuSandwichhinzufuegen("gesund", 1);
        builder.zutatZuSandwichhinzufuegen("gesund", 2);
        builder.zutatZuSandwichhinzufuegen("normal", 0);
        builder.zutatZuSandwichhinzufuegen("normal", 3);
        builder.zutatZuSandwichhinzufuegen("normal", 4);
    }

}