const Weissbrot = artifacts.require("Weissbrot");
const Vollkornbrot = artifacts.require("Vollkornbrot");
const Salat = artifacts.require("Salat");
const Fleisch = artifacts.require("Fleisch");
const Sandwich = artifacts.require("Sandwich");
const SandwichBuilder = artifacts.require("SandwichBuilder");
const Client = artifacts.require("Client");
const Beispiel = artifacts.require("Beispiel");

module.exports = function (deployer) {
  deployer.deploy(Weissbrot);
  deployer.deploy(Vollkornbrot);
  deployer.deploy(Salat);
  deployer.deploy(Fleisch);
  deployer.deploy(Sandwich, "first");
  deployer.deploy(SandwichBuilder);
  deployer.deploy(Client);
  deployer.deploy(Beispiel);
};
