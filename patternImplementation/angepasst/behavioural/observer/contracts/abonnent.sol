pragma solidity ^0.5.0;

import "./zeitung.sol";
import "./verlag.sol";


///@title Repräsentiert den Abonnenten eines verlages
///@author Tobias Wamhof
contract Abonnent {
    string name;
    Zeitung aktuelleZeitung;

    constructor(string memory _name, Verlag _verlag) public {
        name = _name;
        _verlag.aboHinzufuegen(this);
    }

    ///@notice Weist dem Abonennten eines neue Zeitung zu
    ///@param _zeitung neue Zeitung
    function erhalteZeitung(Zeitung _zeitung) public;

    ///@notice Liefert aktuelle Version der Nachricht, die beim Abonennten angekommen ist
    ///@return aktuellste erhaltene Zeitung
    function getAktuelleZeitung() public view returns(Zeitung){
        return aktuelleZeitung;
    }
}
