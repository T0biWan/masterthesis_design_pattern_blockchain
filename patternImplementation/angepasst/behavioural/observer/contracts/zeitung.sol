pragma solidity ^0.5.0;

///@title Repräsentiert eine Zeitung
///@author Tobias Wamhof
contract Zeitung {
    string titel;

    constructor(string memory _titel) public {
        titel = _titel;
    }

    ///@notice Liefert den Titel der Zeitung
    ///@return Titel der Zeitung
    function getTitel() public view returns (string memory) {
        return titel;
    }
}
