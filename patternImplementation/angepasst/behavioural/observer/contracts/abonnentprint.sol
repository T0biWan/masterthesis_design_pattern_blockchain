pragma solidity ^0.5.0;

import "./abonnent.sol";
import "./zeitung.sol";
import "./verlag.sol";

///@title Repräsentiert einen Abonennten der Print Version einer Zeitung
///@author Tobias Wamhof
contract AbonnentPrint is Abonnent {
    event PrintEvent(string);

    constructor(string memory _name, Verlag _verlag) public Abonnent(_name, _verlag){}

    ///@notice Weist dem Abonennten eines neue Zeitung zu
    ///@param _zeitung neue Zeitung
    function erhalteZeitung(Zeitung _zeitung) public {
        aktuelleZeitung = _zeitung;
        emit PrintEvent(
            string(
                abi.encodePacked(name, " erhält Zeitung ", _zeitung.getTitel())
            )
        );
    }
}
