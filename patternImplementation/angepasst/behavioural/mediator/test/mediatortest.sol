pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/chatroom.sol";
import "../contracts/person.sol";
import "../contracts/bot.sol";
import "../contracts/chatteilnehmer.sol";

///@title Dient dem Testen des Mediator Patterns
///@author Tobias Wamhof
contract MediatorTest {
    ChatRoom room = ChatRoom(DeployedAddresses.ChatRoomImpl());
    ChatTeilnehmer real = Person(DeployedAddresses.Person());
    ChatTeilnehmer bot = Bot(DeployedAddresses.Bot());

    ///@notice Testet das hinzufügen und Entfernen von realen Teilnehmern
    function testeHinzufuegenUndEntfernenVonTeilnehmer() public {
        room.kickAll();
        Assert.equal(room.getSizeTeilnehmer(), 0, "Should be initial empty.");

        room.teilnehmerHinzufuegen(real);

        Assert.equal(room.getSizeTeilnehmer(), 1, "Should be increased.");

        room.teilnehmerEntfernen(real);

        Assert.equal(room.getSizeTeilnehmer(), 1, "Should be not reduced.");
    }

    ///@notice Testet das hinzufügen und Entfernen von Bots
    function testeHinzufuegenUndEntfernenVonBot() public {
        room.kickAll();
        Assert.equal(room.getSizeBots(), 0, "Should be initial empty.");

        room.botHinzufuegen(bot);

        Assert.equal(room.getSizeBots(), 1, "Should be increased.");

        room.botEntfernen(bot);

        Assert.equal(room.getSizeBots(), 1, "Should be not reduced.");
    }

    ///@notice Testet das Senden und Empfangen von Nachrichten
    function testeSenden() public {
        room.kickAll();
        room.botHinzufuegen(bot);
        room.teilnehmerHinzufuegen(real);

        real.sendeNachricht("Hallo");

        Assert.equal(bot.getAktuellsteNachricht(), "Hallo", "Should be the same.");
        Assert.equal(real.getAktuellsteNachricht(), "Hallo", "Should be the same.");

        bot.sendeNachricht("BeepBoop");

        Assert.equal(bot.getAktuellsteNachricht(), "Hallo", "Should be the same.");
        Assert.equal(real.getAktuellsteNachricht(), "BeepBoop", "Should be the same.");
    }
}
