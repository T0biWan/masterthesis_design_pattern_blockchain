const ChatRoomImpl = artifacts.require("ChatRoomImpl");
const Person = artifacts.require("Person");
const Bot = artifacts.require("Bot");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(ChatRoomImpl).then(function () {
    deployer.deploy(Person, ChatRoomImpl.address, "Luke Skywalker");
    return deployer.deploy(Bot, ChatRoomImpl.address, "C3PO").then(function () {
      return deployer.deploy(Client, Bot.address, Person.address, ChatRoomImpl.address);
    });
  });
};
