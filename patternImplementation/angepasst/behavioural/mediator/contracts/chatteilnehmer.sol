pragma solidity ^0.5.0;

import "./chatroom.sol";

///@title Repräsentiert einen Chatteilnehmer
///@author Tobias Wamhof
contract ChatTeilnehmer {
    ChatRoom mediator;
    string name;
    string aktuellsteNachricht;

    constructor(string memory _name) public {
        name = _name;
    }

    ///@notice Sendet eine Nachricht von dem Teilnehmer
    ///@param _nachricht zu sendende Nachricht
    function sendeNachricht(string memory _nachricht) public;

    ///@notice Empfängt eine Nachricht
    ///@param _nachricht Inhalt der Nachricht
    ///@param _absender Absender der Nachricht
    function empfangeNachricht(string memory _nachricht, string memory _absender)
        public;

    ///@notice Liefert die aktuellste Nachricht, die beim Nutzer angekommen ist
    ///@dev Hauptsächlich zu Testzwecken
    ///@return aktuellste Nachricht
    function getAktuellsteNachricht() public view returns (string memory) {
        return aktuellsteNachricht;
    }
}
