pragma solidity ^0.5.0;

import "./person.sol";
import "./bot.sol";
import "./chatroom.sol";

///@title Dient dem testen des Mediator Patterns
///@author Tobias Wamhof
contract Client{

    ChatRoom room;
    Bot bot;
    Person teilnehmer;

    constructor(Bot _bot, Person _person, ChatRoom _chatRoom) public{
        room = _chatRoom;
        bot = _bot;
        teilnehmer = _person;
    }

    ///@notice Testet das Senden einer Nachricht von einem Bot
    ///@param _nachricht zu sendende Nachricht
    function sendeVonBot(string memory _nachricht) public {
        bot.sendeNachricht(_nachricht);
    }

    ///@notice Testet das Senden einer Nachricht von einer realen Person
    ///@param _nachricht zu sendende Nachricht
    function sendeVonPerson(string memory _nachricht) public {
        teilnehmer.sendeNachricht(_nachricht);
    }

    ///@notice Entfernt den bot aus dem Chatraum
    function entferneBot() public {
        room.botEntfernen(bot);
    }

    ///@notice Entfernt die reale person aus dem Chatraum
    function entfernePerson() public {
        room.teilnehmerEntfernen(teilnehmer);
    }

    ///@notice Fügt dem Raum einen Bot hinzu
    function hinzufuegenBot() public {
        room.botHinzufuegen(bot);
    }

    ///@notice Fügt dem Raum eine reale Person hinzu
    function hinzufuegenPerson() public {
        room.teilnehmerHinzufuegen(teilnehmer);
    }

}