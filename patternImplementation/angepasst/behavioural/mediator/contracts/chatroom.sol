pragma solidity ^0.5.0;

import "./chatteilnehmer.sol";

///@title Repräsentiert einen Chatraum
///@author Tobias Wamhof
interface ChatRoom {

    ///@notice Fügt dem Chatraum einen realen Teilnehmer hinzu
    ///@param _teilnehmer hinzuzufügender Teilnehmer
    function teilnehmerHinzufuegen(ChatTeilnehmer _teilnehmer) external;

    ///@notice Fügt dem Chatraum einen Bot hinzu
    ///@param _bot hinzuzufügender Bot
    function botHinzufuegen(ChatTeilnehmer _bot) external;

    ///@notice Sendet eine Nachricht
    ///@param _nachricht zu sendende Nachricht
    ///@param _sender absender der Nachricht
    ///@param _echterTeilnehmer Handelt es sich beim Absender um einen realen Teilnehmer oder einen Bot?
    function senden(
        string calldata _nachricht,
        string calldata _sender,
        bool _echterTeilnehmer
    ) external;

    ///@notice Entfernt einen realen Teilnehmer aus dem Chatraum
    ///@param _teilnehmer zu entfernender Teilnehmer
    function teilnehmerEntfernen(ChatTeilnehmer _teilnehmer) external;

    ///@notice Entfernt einen Bot aus dem Chatraum
    ///@param _bot zu entfernender Bot
    function botEntfernen(ChatTeilnehmer _bot) external;

    ///@notice Liefert die Anzahl realer Teilnehmer im Raum
    ///@return Anzahl
    function getSizeTeilnehmer() external view returns (uint256);

    ///@notice Liefert die Anzahl der Bots im Raum
    ///@return Anzahld er Bots
    function getSizeBots() external view returns (uint256);

    ///@notice Entfernt alle Teilnehmer der Chatraums
    function kickAll() external;
}
