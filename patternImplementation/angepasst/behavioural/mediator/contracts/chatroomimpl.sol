pragma solidity ^0.5.0;

import "./chatroom.sol";

///@title Repräsentiert die Implementation eines Chatraums
///@author Tobias Wamhof
contract ChatRoomImpl is ChatRoom {
    ChatTeilnehmer[] teilnehmerListe;
    ChatTeilnehmer[] botListe;

    event RoomEvent(uint, uint);
    event AddressEvent(address);

    ///@notice Fügt dem Chatraum einen realen Teilnehmer hinzu
    ///@param _teilnehmer hinzuzufügender Teilnehmer
    function teilnehmerHinzufuegen(ChatTeilnehmer _teilnehmer) public {
        for(uint i = 0; i < teilnehmerListe.length; i++){
            emit AddressEvent(address(teilnehmerListe[i]));
            if(address(teilnehmerListe[i]) == address(0)){
                teilnehmerListe[i] = _teilnehmer;
                return;
            }
        }
        teilnehmerListe.push(_teilnehmer);
    }

    ///@notice Fügt dem Chatraum einen Bot hinzu
    ///@param _bot hinzuzufügender Bot
    function botHinzufuegen(ChatTeilnehmer _bot) public {
        for(uint i = 0; i < botListe.length; i++){
            if(address(botListe[i]) == address(0)){
                botListe[i] = _bot;
                return;
            }
        }
        botListe.push(_bot);
    }

    ///@notice Sendet eine Nachricht
    ///@param _nachricht zu sendende Nachricht
    ///@param _sender absender der Nachricht
    ///@param _echterTeilnehmer Handelt es sich beim Absender um einen realen Teilnehmer oder einen Bot?
    function senden(string memory _nachricht, string memory _sender, bool _echterTeilnehmer)
        public
    {
        for (uint256 i = 0; i < teilnehmerListe.length; i++) {
            if(address(teilnehmerListe[i]) != address(0)){
                teilnehmerListe[i].empfangeNachricht(_nachricht, _sender);
            }
        }

        if (_echterTeilnehmer) {
            for (uint256 i = 0; i < botListe.length; i++) {
                if(address(botListe[i]) != address(0)){
                    botListe[i].empfangeNachricht(_nachricht, _sender);
                }
            }
        }
    }

    ///@notice Entfernt einen realen Teilnehmer aus dem Chatraum
    ///@param _teilnehmer zu entfernender Teilnehmer
    function teilnehmerEntfernen(ChatTeilnehmer _teilnehmer) public {
        for(uint i = 0; i < teilnehmerListe.length; i++){
            if(address(_teilnehmer) == address(teilnehmerListe[i])){
                delete teilnehmerListe[i];
            }
        }
    }

    ///@notice Entfernt einen Bot aus dem Chatraum
    ///@param _bot zu entfernender Bot
    function botEntfernen(ChatTeilnehmer _bot) public {
        for(uint i = 0; i < botListe.length; i++){
            if(address(_bot) == address(botListe[i])){
                delete botListe[i];
            }
        }
    }

    ///@notice Entfernt alle Teilnehmer der Chatraums
    function kickAll() external{
        delete teilnehmerListe;
        delete botListe;
    }

    ///@notice Liefert die Anzahl realer Teilnehmer im Raum
    ///@return Anzahl
    function getSizeTeilnehmer() public view returns(uint){
        return teilnehmerListe.length;
    }

    ///@notice Liefert die Anzahl der Bots im Raum
    ///@return Anzahld er Bots
    function getSizeBots() public view returns (uint){
        return botListe.length;
    }
}
