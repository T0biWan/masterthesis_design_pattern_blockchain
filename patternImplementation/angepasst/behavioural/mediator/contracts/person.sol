pragma solidity ^0.5.0;

import "./chatteilnehmer.sol";

///@title Repräsentiert eine reale Person in einem Chatraum
///@author Tobias Wamhof
contract Person is ChatTeilnehmer {
    event PersonEvent(string, string);

    constructor(ChatRoom _mediator, string memory _name) public ChatTeilnehmer(_name){
        mediator = _mediator;
        mediator.teilnehmerHinzufuegen(this);
    }

    ///@notice Sendet eine Nachricht einer realen Person an den Chatraum
    ///@param _nachricht zu sendende Nachricht
    function sendeNachricht(string memory _nachricht) public {
        mediator.senden(_nachricht, name, true);
    }

    ///@notice Empfängt eine Nachricht von dem Chatraum
    ///@param _nachricht empfangene Nachricht
    ///@param _absender Absender der Nachricht
    function empfangeNachricht(string memory _nachricht, string memory _absender)
        public
    {
        aktuellsteNachricht = _nachricht;
        emit PersonEvent(_nachricht, _absender);
    }
}
