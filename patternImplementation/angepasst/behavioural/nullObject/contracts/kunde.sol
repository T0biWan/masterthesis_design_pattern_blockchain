pragma solidity ^0.5.0;

///@title Repräsentiert einen Kunden
///@author Tobias Wamhof
contract Kunde {
    string name;

    constructor(string memory _name) public {
        name = _name;
    }

    ///@notice Liefert den Namen des Kunden
    ///@return Name des Kunden
    function getName() public view returns (string memory) {
        return name;
    }

    ///@notice Initialisiert den Kunden mit übergebenem Namen
    ///@param _name Name des Kunden
    function initialize(string memory _name) public{
        name = _name;
    }
}
