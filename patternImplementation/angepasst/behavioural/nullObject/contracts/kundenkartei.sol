pragma solidity ^0.5.0;

import "./kunde.sol";
import "./echterkunde.sol";
import "./nullkunde.sol";
import "./clonefactory.sol";

///@title Speichert Kunden nach ihren Namen in einem Mapping
///@author Tobias Wamhof
contract KundenKartei is CloneFactory{
    Kunde templateNull = new NullKunde("Template");
    Kunde templateReal = new EchterKunde("Template");

    mapping(string => Kunde) kunden;

    ///@notice Sucht im Mapping nach einem Kunden
    ///@param _name Name des Kunden
    ///@return Ein Kudnenobjekt
    function getKunde(string memory _name) public returns (Kunde) {
        if (address(kunden[_name]) != address(0)) {
            return kunden[_name];
        } else {
            Kunde temp = NullKunde(createClone(address(templateNull)));
            temp.initialize(string(abi.encodePacked(_name," konnte nicht gefunden werden.")));
            return temp;
        }
    }

    ///@notice Fügt dem Mapping einen Kunden hinzu
    ///@param _name Name des Kunden
    function hinzufuegen(string memory _name) public {
        Kunde temp = EchterKunde(createClone(address(templateReal)));
        temp.initialize(_name);
        kunden[_name] = temp;
    }


    ///@notice Entfernt einen Kunden aus dem Mapping
    ///@param _name Name des Kunden
    function loeschen(string memory _name) public{
        delete kunden[_name];
    }
}
