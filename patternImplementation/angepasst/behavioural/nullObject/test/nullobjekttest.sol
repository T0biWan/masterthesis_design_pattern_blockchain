pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/kunde.sol";
import "../contracts/kundenkartei.sol";

///@title Dient dem Testen des Null Object Patterns
///@author Tobias Wamhof
contract NullObjektTest {
    KundenKartei kartei =
        KundenKartei(DeployedAddresses.KundenKartei());

    ///@notice Testet das Abfragen eines vorhandenen Kunden
    function testeAbfrageVonRealemNutzer() public {
        kartei.hinzufuegen("Janne");

        Assert.equal(
            kartei.getKunde("Janne").getName(),
            "Janne",
            "Should have been found."
        );

        kartei.loeschen("Janne");
    }

    ///@notice Testet das Abfragen eines nicht vorhandenen Nutzers
    function testeAbfrageVonNullNutzer() public {
        Assert.equal(
            kartei.getKunde("Janne").getName(),
            "Janne konnte nicht gefunden werden.",
            "Should have not been found."
        );
    }
}
