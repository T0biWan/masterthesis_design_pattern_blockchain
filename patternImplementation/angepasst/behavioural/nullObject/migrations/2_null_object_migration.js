const EchterKunde = artifacts.require("EchterKunde");
const NullKunde = artifacts.require("NullKunde");
const KundenKartei = artifacts.require("KundenKartei");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(EchterKunde, "Blub");
  deployer.deploy(NullKunde, "Blub2");
  deployer.deploy(KundenKartei);
  deployer.deploy(Client);
};
