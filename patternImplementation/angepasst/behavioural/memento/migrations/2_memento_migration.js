const Spielstand = artifacts.require("Spielstand");
const SpielstandListe = artifacts.require("SpielstandListe");
const Spiel = artifacts.require("Spiel");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Spielstand, 0);
  deployer.deploy(SpielstandListe).then(function () {
    return deployer.deploy(Spiel).then(function () {
      return deployer.deploy(Client, Spiel.address, SpielstandListe.address);
    });
  });
};
