pragma solidity ^0.5.0;

///@title Repräsentiert einen Spielstand des Spiels
///@author Tobias Wamhof
contract Spielstand {
    uint256 level;

    constructor(uint256 _level) public {
        level = _level;
    }

    ///@notice liefert das gespeicherte Level
    ///@return das Level
    function getLevel() public view returns (uint256) {
        return level;
    }
}
