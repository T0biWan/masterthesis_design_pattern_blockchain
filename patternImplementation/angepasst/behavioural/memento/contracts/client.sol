pragma solidity ^0.5.0;

import "./spiel.sol";
import "./spielstandliste.sol";

///@title Dient dem Testend es Memento Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string, uint256);

    Spiel spiel;
    SpielstandListe liste;

    constructor(Spiel _spiel, SpielstandListe _liste) public {
        spiel = _spiel;
        liste = _liste;
    }

    ///@notice Speichert das aktuelle Level in einem neuen Spielstand
    function spielstandSpeichern() public {
        liste.spielstandHinzufuegen(spiel.spielstandSpeichern());
    }

    ///@notice erhäht das Level des Spiels
    function naechstesLevel() public {
        spiel.naechstesLevel();
    }

    ///@notice lädt einen Spielstand aus der liste aller Spielstände
    ///@param _index Index des Spielstandes in der Spielstandliste
    function spielstandLaden(uint256 _index) public {
        spiel.spielstandLaden(liste.spielstandLaden(_index));
    }

    ///@notice gibt maximal zu ladenden Index aus
    function getMaxIndex() public {
        emit ClientEvent("max index", liste.getSpielstaendeLength());
    }

    ///@notice gibt aktuelles Level des Spiels aus
    function getLevel() public {
        emit ClientEvent("level", spiel.getLevel());
    }
}
