pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/spiel.sol";
import "../contracts/spielstand.sol";
import "../contracts/spielstandliste.sol";
import "../contracts/spielstand.sol";

///@title Dient dem Testen des Memento Patterns
///@author Tobias Wamhof
contract MementoTest {
    Spiel spiel = Spiel(DeployedAddresses.Spiel());
    SpielstandListe liste =
        SpielstandListe(DeployedAddresses.SpielstandListe());

    ///@notice Testet das Hinzufügen und Entfernen eines Spielstands
    function testeHinzufuegenUndEntfernenVonSpielstand() public {
        Assert.equal(liste.getSpielstaendeLength(), 0, "Should be empty.");

        liste.spielstandHinzufuegen(spiel.spielstandSpeichern());

        Assert.equal(liste.getSpielstaendeLength(), 1, "Should be filled.");

        liste.spielstandEntfernen(0);

        Assert.equal(liste.getSpielstaendeLength(), 0, "Should be empty.");
    }

    ///@notice Testet das Laden eines Spielstands
    function testeLadenVonSpielstand() public {
        Assert.equal(spiel.getLevel(), 0, "Should be level 0.");

        liste.spielstandHinzufuegen(spiel.spielstandSpeichern());
        spiel.naechstesLevel();    

        Assert.equal(spiel.getLevel(), 1, "Should be level 1.");
        Assert.equal(liste.getSpielstaendeLength(), 1, "Should have one entry");
        Assert.equal(liste.spielstandLaden(0).getLevel(), 0, "Should be level 0.");

        spiel.spielstandLaden(liste.spielstandLaden(0));
        

        Assert.equal(spiel.getLevel(), 0, "Should be level 0.");

        liste.spielstandEntfernen(0);
    }

    ///@notice Testet das Laden eines ungültigen Spielstands mit zu niedrigem Index
    function testeladenMitZuNiedrigemIndex() public {
        bytes memory payload =
            abi.encodeWithSignature("spielstandLaden(uint256)", "-1");
        (bool success, bytes memory response) = address(liste).call(payload);

        Assert.equal(success, false, "Should not work.");
    }

    ///@notice Testet das Laden eines ungültigen Spielstands mit zu hohem Index
    function testeladenMitZuHohemIndex() public {
        bytes memory payload =
            abi.encodeWithSignature("spielstandLaden(uint256)", "10");
        (bool success, bytes memory response) = address(liste).call(payload);

        Assert.equal(success, false, "Should not work.");
    }
}
