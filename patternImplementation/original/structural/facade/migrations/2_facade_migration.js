const Stift = artifacts.require("Stift");
const Computer = artifacts.require("Computer");
const BriefmarkenAutomat = artifacts.require("BriefmarkenAutomat");
const Sekretaer = artifacts.require("Sekretaer");
const Client = artifacts.require("Client");
const Drucker = artifacts.require("Drucker");
const Dokument = artifacts.require("Dokument");

module.exports = function (deployer) {
  deployer.deploy(Dokument, 'Test').then(function () {
    return deployer.deploy(Stift).then(function () {
      return deployer.deploy(Drucker).then(function () {
        return deployer.deploy(Computer, Drucker.address).then(function () {
          return deployer.deploy(BriefmarkenAutomat).then(function () {
            return deployer.deploy(Sekretaer, Computer.address, BriefmarkenAutomat.address, Stift.address).then(function () {
              return deployer.deploy(Client, Sekretaer.address);
            });
          });
        });
      });
    });
  });
};
