pragma solidity ^0.5.0;

import "./computer.sol";
import "./briefmarkenautomat.sol";
import "./dokument.sol";
import "./stift.sol";

///@title Sekretär als Facade vor dem Subsystems
///@author Tobias Wamhof
contract Sekretaer {
    Computer computer;
    BriefmarkenAutomat automat;
    Dokument dokument;
    Stift stift;

    constructor(
        Computer _computer,
        BriefmarkenAutomat _automat,
        Stift _stift
    ) public {
        computer = _computer;
        automat = _automat;
        stift = _stift;
    }

    ///@notice Dient dem Schreiben und Versendet eines Dokumentes und ruft Methoden im Subsystem auf
    ///@param _nameDokument der name des Dokuments
    function schreibenUndVersenden(string memory _nameDokument) public {        
        dokument = new Dokument(_nameDokument);
        computer.an();
        computer.drucke(dokument);
        computer.aus();
        stift.unterschreibe(dokument);
        automat.kaufeBriefmarke();
    }
}
