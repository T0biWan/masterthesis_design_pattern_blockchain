pragma solidity ^0.5.0;

import "./sekretaer.sol";

///@title Dient dem testen des Faces Patterns
///@author Tobias Wamhof
contract Client {
    Sekretaer sekretaer;

    constructor(Sekretaer _sekretaer) public {
        sekretaer = _sekretaer;
    }

    ///@notice Methode zum Aufruf der Facade
    function testFacade() public {
        sekretaer.schreibenUndVersenden("TestDokument");
    }
}
