pragma solidity ^0.5.0;

import "./dokument.sol";

///@title Drucker als Teil des Subsystems
///@author Tobias Wamhof
contract Drucker {
    event DruckerEvent(string);

    bool eingeschaltet;

    ///@notice Dient dem Einschalten des Druckers
    function an() public {
        eingeschaltet = true;
        emit DruckerEvent("Schalte ein");
    }

    ///@notice Dient dem Ausschalten des Druckers
    function aus() public {
        eingeschaltet = false;
        emit DruckerEvent("Schalte aus");
    }

    ///@notice Methode zum Auffüllen des Papiers
    function papierfuellen() public {
        emit DruckerEvent("Fülle Papier nach");
    }

    ///@notice Dient dem Drucken eines Dokuments
    ///@param _dok zu druckendes Dokument
    function drucke(Dokument _dok) public {
        emit DruckerEvent(
            string(abi.encodePacked("Drucke Dokument: ", _dok.bezeichnung()))
        );
    }
}
