pragma solidity ^0.5.0;

import "./drucker.sol";
import "./dokument.sol";

///@title Computer als Teil des Subsystems
///@author Tobias Wamhof
contract Computer {
    event ComputerEvent(string);

    Drucker drucker;

    bool eingeschaltet;

    constructor(Drucker _drucker) public {
        drucker = _drucker;
    }

    ///@notice Dient dem Einschalten des Computers
    function an() public {
        eingeschaltet = true;
        emit ComputerEvent("Schalte ein");
    }

    ///@notice Dient dem Ausschalten des Computers
    function aus() public {
        eingeschaltet = false;
        emit ComputerEvent("Schalte aus");
    }

    ///@notice Methode zum Aufruf des Druckers
    ///@param _dok zu druckendes Dokument
    function drucke(Dokument _dok) public {
        emit ComputerEvent("Druckauftrag");
        drucker.papierfuellen();
        drucker.an();
        drucker.drucke(_dok);
        drucker.aus();
    }
}
