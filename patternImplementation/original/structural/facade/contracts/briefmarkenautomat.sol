pragma solidity ^0.5.0;

///@title BriefmarkenAutomat als Teil des Subsystems
///@author Tobias Wamhof
contract BriefmarkenAutomat {
    event AutomatenEvent(string);

    ///@notice Methode zum Kaufen einer Briefmarke
    function kaufeBriefmarke() public {
        emit AutomatenEvent("Kaufe Briefmarke");
    }
}
