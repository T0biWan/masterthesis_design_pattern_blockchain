pragma solidity ^0.5.0;

import "./dokument.sol";

///@title Stift als Teil des Subsystems
///@author Tobias Wamhof
contract Stift {
    event StiftEvent(string);

    ///@notice Dient dem Unterschrieben eines Dokuments
    ///@param _dok zu unterschreibendes Dokument
    function unterschreibe(Dokument _dok) public {
        emit StiftEvent(
            string(abi.encodePacked("Unterschreibe ", _dok.bezeichnung()))
        );
    }
}
