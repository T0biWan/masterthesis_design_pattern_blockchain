pragma solidity ^0.5.0;

///@title Dokument als Teil des Subsystems
///@author Tobias Wamhof
contract Dokument {
    string public bezeichnung;

    constructor(string memory _bezeichnung) public {
        bezeichnung = _bezeichnung;
    }
}
