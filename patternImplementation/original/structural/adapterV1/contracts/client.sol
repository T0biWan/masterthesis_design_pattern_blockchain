pragma solidity ^0.5.0;

import "./form.sol";

///@title Dient dem Testen des Adapter Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string);

    Form form;

    constructor(address _address) public {
        form = Form(_address);
    }

    ///@notice Testet den Adapter
    function testeDarstellung() public {
        emit ClientEvent(form.darstellen(1, 2, 2, 3));
    }
}
