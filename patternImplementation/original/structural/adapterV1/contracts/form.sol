pragma solidity ^0.5.0;

///@title Gibt als Interface die Struktur des Adapters vor
///@author Tobias Wamhof
interface Form {

    ///@notice Neue Methode zum Darstellen einer Form
    ///@param . Startpunkt, Höhe und Breite der Form
    ///@return String zu Testzwecken
    function darstellen(
        uint256 x,
        uint256 y,
        uint256 breite,
        uint256 hoehe
    ) external returns (string memory);
}
