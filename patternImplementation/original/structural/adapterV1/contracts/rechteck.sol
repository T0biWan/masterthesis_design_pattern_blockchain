pragma solidity ^0.5.0;

import "./form.sol";

///@title Neue Implementation der Form, die den Adapter darstellt
///@author Tobias Wamhof
contract Rechteck is Form {
    AltesRechteckInterface altesRechteckContract;

    constructor(address _address) public {
        altesRechteckContract = AltesRechteckInterface(_address);
    }

    ///@notice Setzt Adresse des schon auf der Blockchain befindlichen Contracts
    function setAltesRechteckAddresse(address _address) public {
        altesRechteckContract = AltesRechteckInterface(_address);
    }

    ///@notice Neue Methode zum Darstellen einer Form
    ///@param . Startpunkt, Höhe und Breite der Form
    ///@return String zu Testzwecken
    function darstellen(
        uint256 _x,
        uint256 _y,
        uint256 _breite,
        uint256 _hoehe
    ) external returns (string memory) {
        string memory result = altesRechteckContract.darstellen(
            _x,
            _x + _breite,
            _y,
            _y + _hoehe
        );
        return string(abi.encodePacked(result, " ANGEPASST."));
    }
}

///@title Benötigtes Interface zum Zugriff auf bereits deployten Contract
///@author Tobias Wamhof
contract AltesRechteckInterface {

    ///@notice Methode zur Darstellung im alten Rechteck
    ///@param . Punkte der Ecken unten links und oben rechts
    ///@return String zu Testzwecken
    function darstellen(
        uint256 _x1,
        uint256 _x2,
        uint256 _y1,
        uint256 _y2
    ) public pure returns (string memory);
}
