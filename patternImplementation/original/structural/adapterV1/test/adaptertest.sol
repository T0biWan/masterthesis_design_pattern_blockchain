pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/rechteck.sol";
import "../contracts/altesrechteck.sol";

///@title Dient dem Test des Adapter Patterns
///@author Tobias Wamhof
contract AdapterTest {

   string rechteckTest = "Stelle Rechteck mit Eckpunkten dar";
   string adapterTest = " ANGEPASST.";

   Rechteck adapter = Rechteck(DeployedAddresses.Rechteck());
   AltesRechteck rechteck = AltesRechteck(DeployedAddresses.AltesRechteck());

   ///@notice Testet Ausgabe des alten Rechtecks
   function testeAltesRechteck() public {
      Assert.equal(rechteck.darstellen(1, 1, 1, 1), rechteckTest, "Should be the same.");
   }

   ///@notice Testet Ausgabe des Adapters
   function testeAdapter() public {
      Assert.equal(adapter.darstellen(1, 1, 1, 1), string(abi.encodePacked(rechteckTest, adapterTest)), "Should be the same.");
   }
   
}