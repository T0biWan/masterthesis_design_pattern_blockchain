const AltesRechteck = artifacts.require("AltesRechteck");
const Rechteck = artifacts.require("Rechteck");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(AltesRechteck).then(function() {
    return deployer.deploy(Rechteck, AltesRechteck.address).then(function(){
      return deployer.deploy(Client, Rechteck.address);
    });
  });
};
