const AtomarerMitarbeiter = artifacts.require("AtomarerMitarbeiter");
const AbteilungsLeiter = artifacts.require("AbteilungsLeiter");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(AtomarerMitarbeiter, "Example");
  deployer.deploy(AbteilungsLeiter, "Example");
  deployer.deploy(Client);
};
