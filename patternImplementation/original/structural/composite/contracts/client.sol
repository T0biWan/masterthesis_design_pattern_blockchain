pragma solidity ^0.5.0;

import "./mitarbeiter.sol";
import "./atomarermitarbeiter.sol";
import "./abteilungsleiter.sol";

///@title Dient dem Testend es Composite Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string, uint256);

    AbteilungsLeiter rene = new AbteilungsLeiter("Rene");
    AbteilungsLeiter hannah = new AbteilungsLeiter("Hannah");
    AbteilungsLeiter jan = new AbteilungsLeiter("Jan");
    AbteilungsLeiter clemens = new AbteilungsLeiter("Clemens");

    ///@notice Ertstellt eine Hierarchie von Mitarbeitern
    function erstelleStruktur() public {
        AtomarerMitarbeiter olaf = new AtomarerMitarbeiter("Olaf");
        AtomarerMitarbeiter angelika = new AtomarerMitarbeiter("Angelika");
        AtomarerMitarbeiter sofie = new AtomarerMitarbeiter("Sofie");
        AtomarerMitarbeiter laura = new AtomarerMitarbeiter("Laura");

        clemens.mitarbeiterHinzufuegen(angelika);
        clemens.mitarbeiterHinzufuegen(sofie);
        clemens.mitarbeiterHinzufuegen(laura);

        jan.mitarbeiterHinzufuegen(olaf);

        rene.mitarbeiterHinzufuegen(hannah);
        rene.mitarbeiterHinzufuegen(jan);
        rene.mitarbeiterHinzufuegen(clemens);
    }

    ///@notice Testet Zählen von Mitarbeitern für Rene
    function testeRene() public {
        emit ClientEvent("Rene", rene.zaehleMitarbeiter());
    }

    ///@notice Testet Zählen von Mitarbeitern für Jan
    function testeJan() public {
        emit ClientEvent("Jan", jan.zaehleMitarbeiter());
    }

    ///@notice Testet Zählen von Mitarbeitern für Hannah
    function testeHannah() public {
        emit ClientEvent("Hannah", hannah.zaehleMitarbeiter());
    }

    ///@notice Testet Zählen von Mitarbeitern für Clemens
    function testeClemens() public {
        emit ClientEvent("Clemens", clemens.zaehleMitarbeiter());
    }
}
