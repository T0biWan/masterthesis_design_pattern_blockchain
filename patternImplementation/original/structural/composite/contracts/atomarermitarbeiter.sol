pragma solidity ^0.5.0;

import "./mitarbeiter.sol";

///@title Repräsentiert einen Mitarbeiter ohne Untergebene
///@author Tobias Wamhof
contract AtomarerMitarbeiter is Mitarbeiter {
    string public name;

    constructor(string memory _name) public {
        name = _name;
    }

    ///@notice Liefert Anzahl der Untergebenen, also immer 1
    ///@return 1
    function zaehleMitarbeiter() public returns (uint256) {
        return 1;
    }

    ///@notice Liefert Namen des Mitarbeiters
    ///@return den Namen
    function getName() public returns (string memory) {
        return name;
    }
}
