pragma solidity ^0.5.0;

import "./mitarbeiter.sol";

///@title Repräsentiert einen Abteilungsleiter mit unterstellten Mitarbeitern
///@author Tobias Wamhof
contract AbteilungsLeiter is Mitarbeiter {
    Mitarbeiter[] untergebene;

    string public name;

    constructor(string memory _name) public {
        name = _name;
    }

    ///@notice Zählt die untergebenen Mitarbeiter eines Abteilungsleiters
    ///@return die Anzahl seiner Mitarbeiter
    function zaehleMitarbeiter() public returns (uint256) {
        uint256 counter = 1;
        for (uint256 i = 0; i < untergebene.length; i++) {
            counter += untergebene[i].zaehleMitarbeiter();
        }
        return counter;
    }

    ///@notice Fügt einen untergebenen Mitarbeiter hinzu
    ///@param _mitarbeiter hinzuzufügender Mitarbeiter
    function mitarbeiterHinzufuegen(Mitarbeiter _mitarbeiter) public {
        untergebene.push(_mitarbeiter);
    }

    ///@notice Liefert den Namen des Abteilungsleiters
    ///@return der Name
    function getName() public returns (string memory) {
        return name;
    }
}
