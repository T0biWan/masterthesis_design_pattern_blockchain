pragma solidity ^0.5.0;

///@title Gibt die Methoden vor, die jeder Mitarbeiter bereitstellen muss
///@author Tobias Wamhof
contract Mitarbeiter {

    ///@notice Liefert die Anzahl der untergebenen Mitarbeiter
    ///@return die Anzahl
    function zaehleMitarbeiter() public returns (uint256);

    ///@notice Liefert den Namen des Mitarbeiters
    ///@return der Name
    function getName() public returns (string memory);
}
