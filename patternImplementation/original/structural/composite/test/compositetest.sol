pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/abteilungsleiter.sol";
import "../contracts/mitarbeiter.sol";
import "../contracts/atomarermitarbeiter.sol";

///@title Dient dem Testend es Composite Patterns
///@author Tobias Wamhof
contract CompositeTest {
    AbteilungsLeiter rene;
    AbteilungsLeiter hannah;
    AbteilungsLeiter jan;
    AbteilungsLeiter clemens;

    AtomarerMitarbeiter olaf;
    AtomarerMitarbeiter angelika;
    AtomarerMitarbeiter sofie;
    AtomarerMitarbeiter laura;

    ///@notice Testet Zählen von Mitarbeitern für Rene
    function testeRene() public {
        init();
        Assert.equal(rene.zaehleMitarbeiter(), 8, "Should be the same.");
    }

    ///@notice Testet Zählen von Mitarbeitern für Hannah
    function testeHannah() public {
        init();
        Assert.equal(hannah.zaehleMitarbeiter(), 1, "Should be the same.");
    }

    ///@notice Testet Zählen von Mitarbeitern für Jan
    function testeJan() public {
        init();
        Assert.equal(jan.zaehleMitarbeiter(), 2, "Should be the same.");
    }

    ///@notice Testet Zählen von Mitarbeitern für Clemens
    function testeClemens() public {
        init();
        Assert.equal(clemens.zaehleMitarbeiter(), 4, "Should be the same.");
    }

    ///@notice Ertstellt eine Hierarchie von Mitarbeitern
    function init() private {
        rene = new AbteilungsLeiter("Rene");
        hannah = new AbteilungsLeiter("Hannah");
        jan = new AbteilungsLeiter("Jan");
        clemens = new AbteilungsLeiter("Clemens");

        olaf = new AtomarerMitarbeiter("Olaf");
        angelika = new AtomarerMitarbeiter("Angelika");
        sofie = new AtomarerMitarbeiter("Sofie");
        laura = new AtomarerMitarbeiter("Laura");
        clemens.mitarbeiterHinzufuegen(angelika);
        clemens.mitarbeiterHinzufuegen(sofie);
        clemens.mitarbeiterHinzufuegen(laura);

        jan.mitarbeiterHinzufuegen(olaf);

        rene.mitarbeiterHinzufuegen(hannah);
        rene.mitarbeiterHinzufuegen(jan);
        rene.mitarbeiterHinzufuegen(clemens);
    }
}
