const SpezielleFernbedienung = artifacts.require("SpezielleFernbedienung");
const TV = artifacts.require("TV");
const Radio = artifacts.require("Radio");

module.exports = function (deployer) {
  deployer.deploy(Radio);
  deployer.deploy(TV).then(function(){
    return deployer.deploy(SpezielleFernbedienung, TV.address);
  })
};
