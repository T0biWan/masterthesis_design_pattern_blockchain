pragma solidity ^0.5.0;

///@title Gerät das mit einer Fernbedienung bedient werden kann
///@author Tobias Wamhof
contract Geraet{

    bool private eingeschaltet = false;
    uint private lautstaerke = 50;
    uint private kanal = 1;

    ///@notice Liefert einen boolean der aussagt, ob das Gerät ein- oder ausgeschaltet ist
    ///@return true falls eingeschaltet, false falls nicht
    function istEingeschaltet() public view returns(bool){
        return eingeschaltet;
    }

    ///@notice Schaltet Gerät ein
    function einschalten() public {
        eingeschaltet = true;
    }

    ///@notice Schaltet Gerät aus
    function ausschalten() public {
        eingeschaltet = false;
    }

    ///@notice Liefert Lautstärke des Geräts
    ///@return die Lautstärke
    function getLautstaerke() public view returns(uint){
        return lautstaerke;
    }

    ///@notice Setzt Laustärke des Geräts
    ///@param _lautstaerke neue Lautstärke
    function setLautstaerke(uint _lautstaerke) public{
        require(_lautstaerke >= 0 && _lautstaerke <= 100);
        lautstaerke = _lautstaerke;
    }

    ///@notice Liefert Kanal des Geräts
    ///@return der Kanal
    function getKanal() public view returns(uint){
        return kanal;
    }

    ///@notice Setzt Kanal des Geräts
    ///@param _kanal neuer Kanal
    function setKanal(uint _kanal) public{
        require(_kanal >= 0 && _kanal <= 100);
        kanal = _kanal;
    }
    
}