pragma solidity ^0.5.0;

import "./fernbedienung.sol";
import "./tv.sol";
import "./geraet.sol";

///@title Spezielle Fernbedienung für TV Geräte
///@author Tobias Wamhof
contract SpezielleFernbedienung is Fernbedienung{

    constructor (TV _tv) public Fernbedienung(_tv){

    }

    ///@notice Ändert Farbeinstellung des TV Gerätes
    function farbeinstellungAendern() public {
        TV tv = TV(address(geraet));
        tv.setFarbeinstellung(!tv.getFarbeistellung());
    }
}