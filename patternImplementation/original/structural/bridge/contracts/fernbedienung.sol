pragma solidity ^0.5.0;

import "./geraet.sol";

///@title Eine Fernbedienung zur Bedienung von Geräten
///@author Tobias Wamhof
contract Fernbedienung{
    Geraet geraet;

    constructor(Geraet _geraet) public {
        geraet = _geraet;
    }

    ///@notice Schaltet Gerät ein
    function einschalten() public {
        geraet.einschalten();
    }

    ///@notice Schaltet Gerät aus
    function ausschalten() public {
        geraet.ausschalten();
    }

    ///@notice Schaltet Gerät stumm
    function stummschalten() public{
        geraet.setLautstaerke(0);
    }

    ///@notice Erhöht die Lautstärke des Gerätes
    function lauter() public{
        geraet.setLautstaerke(geraet.getLautstaerke() + 1);
    }

    ///@notice Verringert die Lautstärke des Gerätes
    function leiser() public{
        geraet.setLautstaerke(geraet.getLautstaerke() - 1);
    }

    ///@notice Wählt nächsten Kanal des Gerätes
    function naechsterKanal() public{
        geraet.setKanal(geraet.getKanal() + 1);
    }

    ///@notice Wählt vorherigen Kanal des Gerätes
    function vorherigerKanal() public{
        geraet.setKanal(geraet.getKanal() - 1);
    }

}