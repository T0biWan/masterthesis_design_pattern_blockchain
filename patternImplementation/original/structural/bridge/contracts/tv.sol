pragma solidity ^0.5.0;

import "./geraet.sol";

///@title Spezielle Form des Gerätes
///@author Tobias Wamhof
contract TV is Geraet{

    bool private schwarzWeiss;

    ///@notice Setzt Farbeinstellung des TV Gerätes
    ///@param _farbeinstellung neue Farbeinstellung
    function setFarbeinstellung(bool _farbeinstellung) public {
        schwarzWeiss = _farbeinstellung;
    }

    ///@notice Liefert Farbeinstellung des TV Gerätes
    ///@return aktuelle Farbeinstellung
    function getFarbeistellung() public view returns (bool){
        return schwarzWeiss;
    }
    
}