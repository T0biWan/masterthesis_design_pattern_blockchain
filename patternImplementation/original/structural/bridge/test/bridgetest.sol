pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/tv.sol";
import "../contracts/speziellefernbedienung.sol";

///@title Dient dem Testen des Bridge Patterns
///@author Tobias Wamhof
contract BridgeTest{
    SpezielleFernbedienung fb = SpezielleFernbedienung(DeployedAddresses.SpezielleFernbedienung());
    TV tv = TV(DeployedAddresses.TV());

    ///@notice Testet das Einschalten von Geräten
    function testeEinschalten() public{
        tv.ausschalten();

        Assert.equal(tv.istEingeschaltet(), false, "Should be turned off.");

        fb.einschalten();

        Assert.equal(tv.istEingeschaltet(), true, "Should be turned on.");
    }

    ///@notice Testet das Ausschalten von Geräten
    function testeAusschalten() public{
        tv.einschalten();

        Assert.equal(tv.istEingeschaltet(), true, "Should be turned on.");

        fb.ausschalten();

        Assert.equal(tv.istEingeschaltet(), false, "Should be turned off.");
    }

    ///@notice Testet das Stummschalten von Geräten
    function testeStummschalten() public {
        tv.setLautstaerke(50);

        Assert.equal(tv.getLautstaerke(), 50, "Should be set to 50.");

        fb.stummschalten();

        Assert.equal(tv.getLautstaerke(), 0, "Should be set to 0.");
    }

    ///@notice Testet das Valide Erhöhen der Lautstärke von Geräten
    function testeLauterMitValiderEingabe() public{
        tv.setLautstaerke(50);

        Assert.equal(tv.getLautstaerke(), 50, "Should be set to 50.");

        fb.lauter();

        Assert.equal(tv.getLautstaerke(), 51, "Should be set to 51.");
    }

    ///@notice Testet das Valide Vermindern der Lautstärke von Geräten
    function testeLeiserMitValiderEingabe() public{
        tv.setLautstaerke(50);

        Assert.equal(tv.getLautstaerke(), 50, "Should be set to 50.");

        fb.leiser();

        Assert.equal(tv.getLautstaerke(), 49, "Should be set to 49.");
    }

    ///@notice Testet das Valide Erhöhen des Kanals von Geräten
    function testeNaechsterKanalMitValiderEingabe() public{
        tv.setKanal(50);

        Assert.equal(tv.getKanal(), 50, "Should be set to 50.");

        fb.naechsterKanal();

        Assert.equal(tv.getKanal(), 51, "Should be set to 51.");
    }

    ///@notice Testet das Valide Vermindern der Lautstärke von Geräten
    function testeVorherigerKanalMitValiderEingabe() public{
        tv.setKanal(50);

        Assert.equal(tv.getKanal(), 50, "Should be set to 50.");

        fb.vorherigerKanal();

        Assert.equal(tv.getKanal(), 49, "Should be set to 51.");
    }

    ///@notice Testet das Invalide Erhöhen der Lautstärke von Geräten
    function testeLauterMitInvaliderEingabe() public{
        tv.setLautstaerke(100);

        Assert.equal(tv.getLautstaerke(), 100, "Should be set to 100.");

        bytes memory payload = abi.encodeWithSignature("lauter()");
        (bool success, bytes memory response) = address(fb).call(payload);

        Assert.equal(success, false, "Should be not successfull.");
        Assert.equal(tv.getLautstaerke(), 100, "Should still be set to 100.");
    }

    ///@notice Testet das Invalide Verringern der Lautstärke von Geräten
    function testeLeiserMitInvaliderEingabe() public{
        tv.setLautstaerke(0);

        Assert.equal(tv.getLautstaerke(), 0, "Should be set to 0.");

        bytes memory payload = abi.encodeWithSignature("leiser()");
        (bool success, bytes memory response) = address(fb).call(payload);

        Assert.equal(success, false, "Should be not successfull.");
        Assert.equal(tv.getLautstaerke(), 0, "Should still be set to 0.");
    }

    ///@notice Testet das Invalide Erhöhen des Kanals von Geräten
    function testeNaechsterKanalMitInvaliderEingabe() public{
        tv.setKanal(100);

        Assert.equal(tv.getKanal(), 100, "Should be set to 100.");

        bytes memory payload = abi.encodeWithSignature("naechsterKanal()");
        (bool success, bytes memory response) = address(fb).call(payload);

        Assert.equal(success, false, "Should be not successfull.");
        Assert.equal(tv.getKanal(), 100, "Should still be set to 100.");
    }

    ///@notice Testet das Invalide Verringern des Kanals von Geräten
    function testeVorherigerKanalMitInvaliderEingabe() public{
        tv.setKanal(0);

        Assert.equal(tv.getKanal(), 0, "Should be set to 0.");

        bytes memory payload = abi.encodeWithSignature("vorherigerKanal()");
        (bool success, bytes memory response) = address(fb).call(payload);

        Assert.equal(success, false, "Should be not successfull.");
        Assert.equal(tv.getKanal(), 0, "Should still be set to 0.");
    }

}