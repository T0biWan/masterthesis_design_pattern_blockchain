pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/anspruchsvollesobjekt.sol";
import "../contracts/anspruchsvollesobjektimpl.sol";
import "../contracts/anspruchsvollesobjektproxy.sol";

///@title Dient dem Testen des Proxy Patterns
///@author Tobias Wamhof
contract ProxyTest {
    string proxytest = " mit zusätzlicher Logik.";
    string impltest = "Tue etwas.";

    AnspruchsvollesObjekt objekt = new AnspruchsvollesObjektImpl();
    AnspruchsvollesObjekt proxy = new AnspruchsvollesObjektProxy(objekt);

    ///@notice Testen der Methode der Implementation
    function testeImplementation() public {
        Assert.equal(objekt.methode(), impltest, "Should be the same");
    }

    ///@notice Testen der Methode des Proxys
    function testeProxy() public {
        Assert.equal(
            proxy.methode(),
            string(abi.encodePacked(impltest, proxytest)),
            "Should be the same"
        );
    }
}
