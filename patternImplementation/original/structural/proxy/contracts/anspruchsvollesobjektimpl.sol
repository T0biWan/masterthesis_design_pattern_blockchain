pragma solidity ^0.5.0;

import "./anspruchsvollesobjekt.sol";

///@title Konkrete Implementation des Interfaces
///@author Tobias Wamhof
contract AnspruchsvollesObjektImpl is AnspruchsvollesObjekt {

    ///@notice Methode die auf der Implementation ausgeführt wird
    ///@return String zu Testzwecken
    function methode() public returns (string memory) {
        return "Tue etwas.";
    }
}
