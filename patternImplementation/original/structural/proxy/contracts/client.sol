pragma solidity ^0.5.0;

import "./anspruchsvollesobjekt.sol";

///@title Dient dem Testen des Proxy Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string);

    AnspruchsvollesObjekt proxy;

    AnspruchsvollesObjekt impl;

    constructor(AnspruchsvollesObjekt _proxy, AnspruchsvollesObjekt _impl)
        public
    {
        proxy = _proxy;
        impl = _impl;
    }

    ///@notice Testen der Methode des Proxys
    function testProxy() public {
        emit ClientEvent(proxy.methode());
    }

    ///@notice Testen der Methode der Implementation
    function testImpl() public {
        emit ClientEvent(impl.methode());
    }
}
