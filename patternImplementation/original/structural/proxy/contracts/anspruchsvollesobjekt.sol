pragma solidity ^0.5.0;

///@title Interface, das Proxy und Implementation gemeinsam haben
///@author Tobias Wamhof
interface AnspruchsvollesObjekt {

    ///@notice Methode die sowohl von Proxy als auch von Implementation bereitgestellt werden muss
    ///@return String zu Testzwecken
    function methode() external returns (string memory);
}
