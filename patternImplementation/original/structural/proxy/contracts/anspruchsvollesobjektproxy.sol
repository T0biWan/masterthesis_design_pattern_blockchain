pragma solidity ^0.5.0;

import "./anspruchsvollesobjekt.sol";
import "./anspruchsvollesobjektimpl.sol";

///@title Proxy, der vor der Implementation steht
///@author Tobias Wamhof
contract AnspruchsvollesObjektProxy is AnspruchsvollesObjekt {
    AnspruchsvollesObjekt konkretesObjekt;

    constructor(AnspruchsvollesObjekt _anspruchsvollesObjekt) public {
        konkretesObjekt = _anspruchsvollesObjekt;
    }

    ///@notice Methode, die die konkrete Implementation aufurft und den Rückgabewert anpasst
    ///@return String zu Testzwecken
    function methode() public returns (string memory) {
        return
            string(
                abi.encodePacked(
                    konkretesObjekt.methode(),
                    " mit zusätzlicher Logik."
                )
            );
    }
}
