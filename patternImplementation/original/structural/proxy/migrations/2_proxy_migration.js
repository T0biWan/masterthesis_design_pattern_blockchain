const AnspruchsvollesObjektImpl = artifacts.require("AnspruchsvollesObjektImpl");
const AnspruchsvollesObjektProxy = artifacts.require("AnspruchsvollesObjektProxy");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(AnspruchsvollesObjektImpl).then(function() {
    return deployer.deploy(AnspruchsvollesObjektProxy, AnspruchsvollesObjektImpl.address).then(function(){
      return deployer.deploy(Client, AnspruchsvollesObjektProxy.address, AnspruchsvollesObjektImpl.address);
    });
  });
};
