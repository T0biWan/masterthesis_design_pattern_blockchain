const AltesRechteck = artifacts.require("AltesRechteck");
const Rechteck = artifacts.require("Rechteck");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(AltesRechteck);
  deployer.deploy(Rechteck);
  deployer.deploy(Client);
};
