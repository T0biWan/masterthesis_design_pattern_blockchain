pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/rechteck.sol";
import "../contracts/altesrechteck.sol";

contract AdapterTest {

   string rechteckTest = "Stelle Rechteck mit Eckpunkten dar";
   string adapterTest = " ANGEPASST.";

   Rechteck adapter = Rechteck(DeployedAddresses.Rechteck());
   AltesRechteck rechteck = AltesRechteck(DeployedAddresses.AltesRechteck());

   function testeAltesRechteck() public {
      Assert.equal(rechteck.darstellen(1, 1, 1, 1), rechteckTest, "Should be the same.");
   }

   function testeAdapter() public {
      Assert.equal(adapter.darstellenNeu(1, 1, 1, 1), string(abi.encodePacked(rechteckTest, adapterTest)), "Should be the same.");
   }
   
}