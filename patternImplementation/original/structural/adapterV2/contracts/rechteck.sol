pragma solidity ^0.5.0;

import "./form.sol";
import "./altesrechteck.sol";

contract Rechteck is Form, AltesRechteck {
    function darstellenNeu(
        uint256 _x,
        uint256 _y,
        uint256 _breite,
        uint256 _hoehe
    ) external returns (string memory) {
        string memory result = darstellen(_x, _x + _breite, _y, _y + _hoehe);
        return string(abi.encodePacked(result, " ANGEPASST."));
    }
}
