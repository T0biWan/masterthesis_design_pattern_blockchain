pragma solidity ^0.5.0;

import "./form.sol";
import "./rechteck.sol";

contract Client {
    event ClientEvent(string);

    Form form = new Rechteck();

    function testeDarstellung() public {
        emit ClientEvent(form.darstellenNeu(1, 2, 2, 3));
    }
}
