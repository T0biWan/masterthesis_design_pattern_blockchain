pragma solidity ^0.5.0;

interface Form {
    function darstellenNeu(
        uint256 x,
        uint256 y,
        uint256 breite,
        uint256 hoehe
    ) external returns (string memory);
}
