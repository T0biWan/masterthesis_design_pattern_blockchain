pragma solidity ^0.5.0;

contract AltesRechteck {
    function darstellen(
        uint256 _x1,
        uint256 _x2,
        uint256 _y1,
        uint256 _y2
    ) public pure returns (string memory) {
        return string(abi.encodePacked("Stelle Rechteck mit Eckpunkten dar"));
    }
}
