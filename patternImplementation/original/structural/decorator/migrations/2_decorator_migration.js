const Steak = artifacts.require("Steak");
const Tofu = artifacts.require("Tofu");
const GerichtMitSalat = artifacts.require("GerichtMitSalat");
const GerichtMitPommes = artifacts.require("GerichtMitPommes");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
    deployer.deploy(Steak).then(function () {
        deployer.deploy(GerichtMitPommes, Steak.address);
        return deployer.deploy(GerichtMitSalat, Steak.address);
    });
    deployer.deploy(Tofu);
    deployer.deploy(Client);
};
