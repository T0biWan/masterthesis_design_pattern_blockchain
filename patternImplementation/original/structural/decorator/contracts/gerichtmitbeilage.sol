pragma solidity ^0.5.0;

import "./gericht.sol";

///@title Gibt Struktur der Contracts vor, die die Hauptgerichte dekorieren sollen
///@author Tobias Wamhof
contract GerichtMitBeilage is Gericht {
    Gericht gericht;

    constructor(Gericht _gericht) public {
        gericht = _gericht;
    }
}
