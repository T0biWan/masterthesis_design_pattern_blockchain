pragma solidity ^0.5.0;

import "./gericht.sol";
import "./steak.sol";
import "./tofu.sol";
import "./gerichtmitbeilage.sol";
import "./gerichtmitsalat.sol";
import "./gerichtmitpommes.sol";

///@title Dient dem Testen des Decorator Patterns
///@author Tobias Wamhof
contract Client {
    Gericht gericht;
    Gericht gerichtMitBeilage;

    event ClientEvent(string, uint256);


    ///@notice Testet Steak
    function testeSteak() public {
        gericht = new Steak();
        emit ClientEvent(gericht.getBezeichnung(), gericht.getPreis());
    }

    ///@notice Testet Steak mit Salat
    function testeSteakMitSalat() public {
        gericht = new Steak();
        gerichtMitBeilage = new GerichtMitSalat(gericht);

        emit ClientEvent(
            gerichtMitBeilage.getBezeichnung(),
            gerichtMitBeilage.getPreis()
        );
    }

    ///@notice Testet Steak mit Pommes
    function testeSteakMitPommes() public {
        gericht = new Steak();
        gerichtMitBeilage = new GerichtMitPommes(gericht);

        emit ClientEvent(
            gerichtMitBeilage.getBezeichnung(),
            gerichtMitBeilage.getPreis()
        );
    }

    ///@notice Testet Tofu
    function testeTofu() public {
        gericht = new Tofu();
        emit ClientEvent(gericht.getBezeichnung(), gericht.getPreis());
    }

    ///@notice Testet Tofu mit Salat
    function testeTofuMitSalat() public {
        gericht = new Tofu();
        gerichtMitBeilage = new GerichtMitSalat(gericht);

        emit ClientEvent(
            gerichtMitBeilage.getBezeichnung(),
            gerichtMitBeilage.getPreis()
        );
    }

    ///@notice Testet Tofu mit Pommes
    function testeTofuMitPommes() public {
        gericht = new Tofu();
        gerichtMitBeilage = new GerichtMitPommes(gericht);

        emit ClientEvent(
            gerichtMitBeilage.getBezeichnung(),
            gerichtMitBeilage.getPreis()
        );
    }
}
