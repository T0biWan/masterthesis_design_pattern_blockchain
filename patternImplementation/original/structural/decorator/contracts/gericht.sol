pragma solidity ^0.5.0;

///@title Gibt Methoden vor, die jedes Gericht implementieren muss
///@author Tobias Wamhof
interface Gericht {

    ///@notice Liefert den Preis des Gerichtes
    ///@return der Preis
    function getPreis() external returns (uint256);

    ///@notice Liefert die Bezeichnung des Gerichts
    ///@return die Bezeichnung
    function getBezeichnung() external returns (string memory);
}
