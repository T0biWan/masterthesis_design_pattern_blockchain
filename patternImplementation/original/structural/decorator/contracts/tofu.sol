pragma solidity ^0.5.0;

import "./gericht.sol";

///@title Repräsentiert das Hauptgericht Tofu
///@author Tobias Wamhof
contract Tofu is Gericht {

    ///@notice Liefert den Preis des Gerichtes
    ///@return der Preis (3)
    function getPreis() external returns (uint256) {
        return 3;
    }

    ///@notice Liefert die Bezeichnung des Gerichts
    ///@return die Bezeichnung (Tofu)
    function getBezeichnung() external returns (string memory) {
        return "Tofu";
    }
}
