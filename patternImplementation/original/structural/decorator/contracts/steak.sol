pragma solidity ^0.5.0;

import "./gericht.sol";

///@title Repräsentiert das Hauptgericht Steak
///@author Tobias Wamhof
contract Steak is Gericht {

    ///@notice Liefert den Preis des Gerichtes
    ///@return der Preis (4)
    function getPreis() external returns (uint256) {
        return 4;
    }

    ///@notice Liefert die Bezeichnung des Gerichts
    ///@return die Bezeichnung (Steak)
    function getBezeichnung() external returns (string memory) {
        return "Steak";
    }
}
