pragma solidity ^0.5.0;

import "./gerichtmitbeilage.sol";

///@title Repräsentiert ein Hauptgericht mit Pommes
///@author Tobias Wamhof
contract GerichtMitSalat is GerichtMitBeilage {
    constructor(Gericht _gericht) public GerichtMitBeilage(_gericht) {
        gericht = _gericht;
    }

    ///@notice Liefert den angepassten Preis des Gerichtes
    ///@return der Preis des Hauptgerichtes plus 1
    function getPreis() external returns (uint256) {
        return gericht.getPreis() + 1;
    }

    ///@notice Liefert die angepasste Bezeichnung des Gerichts
    ///@return die Bezeichnung des Hauptgerichts plus "mit Salat"
    function getBezeichnung() external returns (string memory) {
        return string(abi.encodePacked(gericht.getBezeichnung(), " mit Salat"));
    }
}
