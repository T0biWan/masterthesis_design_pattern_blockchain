pragma solidity ^0.5.0;

import "./gerichtmitbeilage.sol";

///@title Repräsentiert ein Hauptgericht mit Pommes
///@author Tobias Wamhof
contract GerichtMitPommes is GerichtMitBeilage {
    constructor(Gericht _gericht) public GerichtMitBeilage(_gericht) {
        gericht = _gericht;
    }

    ///@notice Liefert den angepassten Preis des Gerichtes
    ///@return der Preis des Hauptgerichtes plus 2
    function getPreis() external returns (uint256) {
        return gericht.getPreis() + 2;
    }

    ///@notice Liefert die angepasste Bezeichnung des Gerichts
    ///@return die Bezeichnung des Hauptgerichts plus "mit Pommes"
    function getBezeichnung() external returns (string memory) {
        return
            string(abi.encodePacked(gericht.getBezeichnung(), " mit Pommes"));
    }
}
