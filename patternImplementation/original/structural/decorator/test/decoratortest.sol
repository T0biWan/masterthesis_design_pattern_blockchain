pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/gericht.sol";
import "../contracts/steak.sol";
import "../contracts/tofu.sol";
import "../contracts/gerichtmitbeilage.sol";
import "../contracts/gerichtmitsalat.sol";
import "../contracts/gerichtmitpommes.sol";

///@title Dient dem Testend es Composite Patterns
///@author Tobias Wamhof
contract DecoratorTest {
    Gericht gericht;
    Gericht gerichtMitBeilage;

    ///@notice Testet Steak
    function testeSteak() public {
        gericht = new Steak();
        Assert.equal(gericht.getPreis(), 4, "Should be the same.");
        Assert.equal(gericht.getBezeichnung(), "Steak", "Should be the same.");
    }

    ///@notice Testet Steak mit Salat
    function testeSteakMitSalat() public {
        gericht = new Steak();
        gerichtMitBeilage = new GerichtMitSalat(gericht);

        Assert.equal(gerichtMitBeilage.getPreis(), 5, "Should be the same.");
        Assert.equal(gerichtMitBeilage.getBezeichnung(), "Steak mit Salat", "Should be the same.");
    }

    ///@notice Testet Steak mit Pommes
    function testeSteakMitPommes() public {
        gericht = new Steak();
        gerichtMitBeilage = new GerichtMitPommes(gericht);

        Assert.equal(gerichtMitBeilage.getPreis(), 6, "Should be the same.");
        Assert.equal(gerichtMitBeilage.getBezeichnung(), "Steak mit Pommes", "Should be the same.");
    }

    ///@notice Testet Tofu
    function testeTofu() public {
        gericht = new Tofu();
        Assert.equal(gericht.getPreis(), 3, "Should be the same.");
        Assert.equal(gericht.getBezeichnung(), "Tofu", "Should be the same.");
    }

    ///@notice Testet Tofu mit Salat
    function testeTofuMitSalat() public {
        gericht = new Tofu();
        gerichtMitBeilage = new GerichtMitSalat(gericht);

        Assert.equal(gerichtMitBeilage.getPreis(), 4, "Should be the same.");
        Assert.equal(gerichtMitBeilage.getBezeichnung(), "Tofu mit Salat", "Should be the same.");
    }

    ///@notice Testet Tofu mit Pommes
    function testeTofuMitPommes() public {
        gericht = new Tofu();
        gerichtMitBeilage = new GerichtMitPommes(gericht);

        Assert.equal(gerichtMitBeilage.getPreis(), 5, "Should be the same.");
        Assert.equal(gerichtMitBeilage.getBezeichnung(), "Tofu mit Pommes", "Should be the same.");
    }
}
