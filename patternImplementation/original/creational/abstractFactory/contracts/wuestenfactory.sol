pragma solidity ^0.5.0;

import "./abstraktterrainfactory.sol";
import "./untergrund.sol";
import "./tier.sol";
import "./sand.sol";
import "./kamel.sol";

///@title Dient dem Erzeugen von Instanzen von Contracts, die zur Wüste gehören
///@author Tobias Wamhof
contract WuestenFactory is AbstraktTerrainFactory {

    ///@notice Erzeugt einen Wüstenspezifischen Untergrund
    ///@return spezifischer Untergrund
    function erstelleUntergrund() external returns (Untergrund) {
        return new Sand();
    }

    ///@notice Erzeugt ein Wüstenspezifisches Tier
    ///@return spezifisches Tier
    function erstelleTier() external returns (Tier) {
        return new Kamel();
    }
}
