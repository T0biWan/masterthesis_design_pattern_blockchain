pragma solidity ^0.5.0;

import "./untergrund.sol";
import "./tier.sol";

///@title Interface für die verschiedenen Arten von Fabriken, die je für ein bestimmtes Terrain zuständig sind
///@author Tobias Wamhof
interface AbstraktTerrainFactory {

    ///@notice Erstellt den Terrain spezifischen Untergrund
    ///@return spezifischer Untergrund
    function erstelleUntergrund() external returns (Untergrund);

    ///@notice Erstellt das Terrain spezifische Tier
    ///@return spezifisches Tier
    function erstelleTier() external returns (Tier);
}
