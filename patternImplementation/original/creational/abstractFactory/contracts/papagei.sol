pragma solidity ^0.5.0;

import "./tier.sol";

///@title Spezielles Tier des Regenwalds
///@author Tobias Wamhof
contract Papagei is Tier {

    ///@notice Gibt eine Beschreibung der erzeugten Instanz aus
    ///@return die Beschreibung
    function ausgabe() external pure returns (string memory) {
        return "Ich bin ein Papagei.";
    }
}
