pragma solidity ^0.5.0;

import "./belag.sol";

///@title Spezielle From des Belags
///@author Tobias Wamhof
contract Fleisch is Belag {
    constructor() public {
        bezeichnung = "Fleisch";
    }
}
