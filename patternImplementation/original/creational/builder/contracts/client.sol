pragma solidity ^0.5.0;

import "./sandwichbuilder.sol";

///@title Dient dem Testen des Builder Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string);

    SandwichBuilder builder = new SandwichBuilder();

    ///@notice Testet die Erstellung eines gesunden Sandwichs
    function testeErstellungGesund() public {
        printSandwich(builder.erstelleGesundesSandwich());
    }

    ///@notice Testet die Erstellung eines normalen Sandwichs
    function testeErstellungNormal() public {
        printSandwich(builder.erstelleNormalesSandwich());
    }

    ///@notice Gibt die Zutaten eines Sandwichs aus
    ///@param _sandwich auszugebendes Sandwich
    function printSandwich(Sandwich _sandwich) private {
        emit ClientEvent(_sandwich.getBezeichnung());

        Zutat[] memory zutaten = _sandwich.getZutaten();
        for (uint256 i = 0; i < zutaten.length; i++) {
            emit ClientEvent(zutaten[i].getBezeichnung());
        }
    }
}
