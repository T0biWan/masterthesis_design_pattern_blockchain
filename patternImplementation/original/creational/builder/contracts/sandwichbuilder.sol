pragma solidity ^0.5.0;

import "./sandwich.sol";
import "./vollkornbrot.sol";
import "./salat.sol";
import "./weissbrot.sol";
import "./fleisch.sol";

///@title Dient dem Erzeugen von verschiedenen Sadnwiches
///@author Tobias Wamhof
contract SandwichBuilder {

    ///@notice Erstellt ein gesundes Sandwich
    ///@return Instanziiertes Sandwich
    function erstelleGesundesSandwich() public returns (Sandwich) {
        Sandwich sandwich = new Sandwich("gesund");

        Zutat[] memory zutaten = new Zutat[](2);
        zutaten[0] = new Salat();
        zutaten[1] = new Vollkornbrot();

        sandwich.zutatenHinzufuegen(zutaten);

        return sandwich;
    }

    ///@notice Erstellt ein normales Sandwich
    ///@return instanziiertes Sandwich
    function erstelleNormalesSandwich() public returns (Sandwich) {
        Sandwich sandwich = new Sandwich("normal");

        Zutat[] memory zutaten = new Zutat[](2);
        zutaten[0] = new Fleisch();
        zutaten[1] = new Weissbrot();

        sandwich.zutatenHinzufuegen(zutaten);

        return sandwich;
    }
}
