pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/sandwichbuilder.sol";
import "../contracts/sandwich.sol";

///@title Dient dem Testen des Builder Patterns
///@author Tobias Wamhof
contract BuilderTest{

    string fleischTest = "Fleisch";
    string salatTest = "Salat";
    string weissbrotTest = "Weissbrot";
    string vollkornbrotTest = "Vollkornbrot";

    SandwichBuilder builder = SandwichBuilder(DeployedAddresses.SandwichBuilder());
    Sandwich result;

    ///@notice Testet die Erstellung eines gesunden Sandwichs
    function testeErstellungGesundesSandwich() public{
        result = builder.erstelleGesundesSandwich();

        Assert.equal(result.getZutaten()[0].getBezeichnung(), salatTest, "Should be the same");
        Assert.equal(result.getZutaten()[1].getBezeichnung(), vollkornbrotTest, "Should be the same");
    }

    ///@notice Testet die Erstellung eines normalen Sandwichs
    function testeErstellungNormalesSandwich() public{
        result = builder.erstelleNormalesSandwich();

        Assert.equal(result.getZutaten()[0].getBezeichnung(), fleischTest, "Should be the same");
        Assert.equal(result.getZutaten()[1].getBezeichnung(), weissbrotTest, "Should be the same");
    }

}