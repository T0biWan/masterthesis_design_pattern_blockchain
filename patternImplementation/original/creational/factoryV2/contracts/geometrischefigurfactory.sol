pragma solidity ^0.5.0;

import "./geometrischefigur.sol";
import "./rechteck.sol";
import "./dreieck.sol";

///@title Erzeugt verschiedene geometrische Figuren
///@author Tobias Wamhof
contract GeometrischeFigurFactory {

    ///@notice Erzeugt je nach übergebenem Parameter eine Implementierung der geometrischen Figur
    ///@param _type typ der zu erzeugenden geometrischen Figur (1=Rechteck, 2=Dreieck)
    ///@return die erzeugte Instanz
    function erzeugeInstanz(uint256 _type) public returns (GeometrischeFigur) {
        require(_type >= 1 && _type < 3);
        if (_type == 1) {
            return new Rechteck();
        } else if (_type == 2) {
            return new Dreieck();
        }
    }
}
