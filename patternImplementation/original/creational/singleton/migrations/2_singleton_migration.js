const Singleton = artifacts.require("Singleton");
const SingletonVerwalter = artifacts.require("SingletonVerwalter");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Singleton);
  deployer.deploy(SingletonVerwalter);
  deployer.deploy(Client);
};
