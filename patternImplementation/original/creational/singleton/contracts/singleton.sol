pragma solidity ^0.5.0;

contract Singleton{
    string public singletonText = "Ich bin ein einzigartigerText";

    constructor () internal{
        
    }

    function setSingletonText(string memory _text)public{
        singletonText = _text;
    }

}