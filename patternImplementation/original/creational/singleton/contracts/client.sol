pragma solidity ^0.5.0;

import "./singletonverwalter.sol";

contract Client{
    event ClientEvent(string); 

    SingletonVerwalter verwalter = new SingletonVerwalter();

    function testSingleton()public{
        Singleton single = verwalter.getInstanz();
        emit ClientEvent(single.singletonText());
        single.setSingletonText("neu");
    }

    function test2()public{
        Singleton single = verwalter.getInstanz();
        emit ClientEvent(single.singletonText());
        single.setSingletonText("neu2");
    }

    function test3()public{
        Singleton single = verwalter.getInstanz();
        emit ClientEvent(single.singletonText());
        single.setSingletonText("neu3");
    }
}