pragma solidity ^0.5.0;

import "./singleton.sol";

contract SingletonVerwalter is Singleton{
    Singleton instanz;

    function getInstanz() public returns (Singleton){
        if(address(instanz) == address(0)){
            instanz = new Singleton();
        }
        return instanz;
    }
}