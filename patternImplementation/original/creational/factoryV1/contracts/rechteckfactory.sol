pragma solidity ^0.5.0;

import "./geometrischefigurfactory.sol";
import "./rechteck.sol";

///@title Dient der Erzeugung von Rechtecken
///@author Tobias Wamhof
contract RechteckFactory is GeometrischeFigurFactory {

    ///@notice Erzeugt eine Rechtecksinstanz
    ///@return das instanziierte Rechteck
    function erzeugeInstanz() public returns (GeometrischeFigur) {
        return new Rechteck();
    }
}
