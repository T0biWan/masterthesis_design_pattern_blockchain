pragma solidity ^0.5.0;

import "./geometrischefigurfactory.sol";
import "./dreieck.sol";

///@title Dient der Erzeugung von Dreiecken
///@author Tobias Wamhof
contract DreieckFactory is GeometrischeFigurFactory {

    ///@notice Erzeugt eine Dreiecksinstanz
    ///@return das instanziierte Dreieck
    function erzeugeInstanz() public returns (GeometrischeFigur) {
        return new Dreieck();
    }
}
