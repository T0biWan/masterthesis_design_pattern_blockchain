const Dreieck = artifacts.require("Dreieck");
const Rechteck = artifacts.require("Rechteck");
const DreieckFactory = artifacts.require("DreieckFactory");
const RechteckFactory = artifacts.require("RechteckFactory");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Dreieck);
  deployer.deploy(Rechteck);
  deployer.deploy(DreieckFactory);
  deployer.deploy(RechteckFactory);
  deployer.deploy(Client);
};
