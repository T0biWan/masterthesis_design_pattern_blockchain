pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/geometrischefigur.sol";
import "../contracts/geometrischefigurfactory.sol";
import "../contracts/dreieckfactory.sol";
import "../contracts/rechteckfactory.sol";

///@title Dient dem Testen des Factory (Method) Patterns
///@author Tobias Wamhof
contract FactoryTest{

    string dreieckTest = "Dreieck";
    string rechteckTest = "Rechteck";

    GeometrischeFigurFactory factory;
    GeometrischeFigur result;

    ///@notice Testet Generierung eines Dreickecks
    function testeErstellungDreieck() public{
        factory = DreieckFactory(DeployedAddresses.DreieckFactory());
        result = factory.erzeugeInstanz();

        Assert.equal(result.getFigurTyp(), dreieckTest, "Should be the same");
    }

    ///@notice Testet Generierung eines Rechtecks
    function testeErstellungRechteck() public{
        factory = RechteckFactory(DeployedAddresses.RechteckFactory());
        result = factory.erzeugeInstanz();

        Assert.equal(result.getFigurTyp(), rechteckTest, "Should be the same");
    }

}