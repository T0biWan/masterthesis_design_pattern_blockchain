pragma solidity ^0.5.0;

///@title Repräsentiert eine im Object Pool verwaltete Instanz
///@author Tobias Wamhof
contract Connection {
    string private host;
    uint private port;

    constructor(string memory _host, uint _port) public {
        host = _host;
        port = _port;
    }

    ///@notice Liefert Host der Verbindung zurück
    ///@return Host der Verbindung
    function getHost() public view returns (string memory) {
        return host;
    }

    ///@notice Liefert Port der Verbindung zurück
    ///@return Port der Verbindung
    function getPort() public view returns (uint) {
        return port;
    }
}
