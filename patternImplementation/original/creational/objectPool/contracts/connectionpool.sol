pragma solidity ^0.5.0;

import "./connection.sol";

///@title Verwaltet eine Liste von Connection Instanzen
///@author Tobias Wamhof
contract ConnectionPool {
    event ConectionHinzugefuegt(uint);
    event AnzahlFrei(uint256);
    event AnzahlBelegt(uint256);

    Connection[] freieVerbindungen;
    Connection[] genutzteVerbindungen;

    uint gesamtZahl;

    constructor(uint _zuErzeugendeVerbindungen) public {
        instanziiere(_zuErzeugendeVerbindungen);
    }

    ///@notice Liefert eine freie Verbindung zurück, falls eine existiert
    ///@return eine freie Verbindung
    function fordereVerbindung() public returns (Connection) {
        require(freieVerbindungen.length > 0);
        Connection verbindung = freieVerbindungen[0];
        fuelleLuecke(0, true);
        genutzteVerbindungen.push(verbindung);
        emit AnzahlFrei(freieVerbindungen.length);
        emit AnzahlBelegt(genutzteVerbindungen.length);
        return verbindung;
    }

    ///@notice Gibt eine Verbindung frei, die zuvor genutzt wurde
    ///@param _connection die genutzte Verbindung
    function gebeFrei(Connection _connection) public {
        require(genutzteVerbindungen.length > 0);
        for (uint256 i = 0; i < genutzteVerbindungen.length; i++) {
            if (address(_connection) == address(genutzteVerbindungen[i])) {
                freieVerbindungen.push(genutzteVerbindungen[i]);
                fuelleLuecke(i, false);
                emit AnzahlFrei(freieVerbindungen.length);
                emit AnzahlBelegt(genutzteVerbindungen.length);
                break;
            }
        }
    }

    ///@notice Füllt die Lücke in einem Array mit dem letzten Eintrag auf und löscht diesen dann
    ///@param _position zu füllende Position
    ///@param _freieVerbindungen in welchem Array soll die Lücke gefüllt werden? (true = freieVerbindungen, false = genutzteVerbindungen)
    function fuelleLuecke(uint256 _position, bool _freieVerbindungen) private {
        if (_freieVerbindungen) {
            freieVerbindungen[_position] = freieVerbindungen[freieVerbindungen
                .length - 1];
            delete freieVerbindungen[freieVerbindungen.length - 1];
            freieVerbindungen.length--;
        } else if (!_freieVerbindungen) {
            genutzteVerbindungen[_position] = genutzteVerbindungen[genutzteVerbindungen
                .length - 1];
            delete genutzteVerbindungen[genutzteVerbindungen.length - 1];
            genutzteVerbindungen.length--;
        }
    }

    ///@notice Setzt Anzahl der zu erzeugenden Instanzen und instanziiert diese
    ///@param _anzahl Anzahl zu erzeugender Instanzen
    function setZuErzeugendeInstanzen(uint _anzahl) public {
        instanziiere(_anzahl);
    }

    ///@notice Instantiiert die übergebe Anzahl an Verbindungen
    ///@param _zuErzeugendeObjekte Anzahl der benötigten Instanzen
    function instanziiere(uint _zuErzeugendeObjekte) private{
        gesamtZahl = _zuErzeugendeObjekte;

        delete freieVerbindungen;
        delete genutzteVerbindungen;

        for (uint i = 0; i < _zuErzeugendeObjekte; i++) {
            Connection con = new Connection("Host", i);
            freieVerbindungen.push(con);
            emit ConectionHinzugefuegt(con.getPort());
        }
    }

    ///@notice Liefert die Anzahl freier Verbindungen
    ///@return Anzahl freier Verbindungen
    function getAnzahlFreierVerbindungen() public view returns(uint){
        return freieVerbindungen.length;
    }
}
