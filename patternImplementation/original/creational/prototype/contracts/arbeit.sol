pragma solidity ^0.5.0;

import "./dienstplanobjekt.sol";

///@title Repräsentiert ein zu klonenedes Arbeitsobjekt
///@author Tobias Wamhof
contract Arbeit is DienstplanObjekt {
    constructor(uint256 _monat) public DienstplanObjekt(_monat) {}

    ///@notice Liefert den Typ des Dienstplanobjektes zurück
    ///@return Typ des Dienstplanobjektes
    function getTyp() public pure returns (string memory) {
        return "Arbeit";
    }
}
