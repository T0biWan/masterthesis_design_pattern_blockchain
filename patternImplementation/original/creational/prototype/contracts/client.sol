pragma solidity ^0.5.0;

import "./dienstplanobjektcloner.sol";
import "./dienstplanobjekt.sol";

///@title Dient dem Testen des Prototype Patterns
///@author Tobias Wamhof
contract Client {
    DienstplanObjekt arbeit1;
    DienstplanObjekt urlaub2;
    DienstplanObjekt arbeit3;

    event DienstplanEvent(uint256, string);

    DienstplanObjektCloner cloner = new DienstplanObjektCloner();

    ///@notice Testet die Generierung und Editierung von Klonen
    function testGenerierung() public {
        arbeit1 = cloner.erzeugeArbeitsObjekt(1);
        urlaub2 = cloner.erzeugeUrlaubsObjekt(2);
        arbeit3 = cloner.erzeugeArbeitsObjekt(3);

        emit DienstplanEvent(arbeit1.getMonat(), arbeit1.getTyp());
        emit DienstplanEvent(urlaub2.getMonat(), urlaub2.getTyp());
        emit DienstplanEvent(arbeit3.getMonat(), arbeit3.getTyp());

        arbeit1.setMonat(4);

        emit DienstplanEvent(arbeit1.getMonat(), arbeit1.getTyp());
        emit DienstplanEvent(urlaub2.getMonat(), urlaub2.getTyp());
        emit DienstplanEvent(arbeit3.getMonat(), arbeit3.getTyp());
    }
}
