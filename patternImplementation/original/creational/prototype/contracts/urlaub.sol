pragma solidity ^0.5.0;

import "./dienstplanobjekt.sol";

///@title Repräsentiert ein zu klonenedes Urlaubsobjekt
///@author Tobias Wamhof
contract Urlaub is DienstplanObjekt {
    constructor(uint256 _monat) public DienstplanObjekt(_monat) {}

    ///@notice Liefert den Typ des Dienstplanobjektes zurück
    ///@return Typ des Dienstplanobjektes
    function getTyp() public pure returns (string memory) {
        return "Urlaub";
    }
}
