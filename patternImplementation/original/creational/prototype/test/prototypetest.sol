pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/dienstplanobjektcloner.sol";
import "../contracts/dienstplanobjekt.sol";

///@title Dient dem Testen des Prototype Patterns
///@author Tobias Wamhof
contract PrototypeTest{

    string arbeitTest = "Arbeit";
    string urlaubTest = "Urlaub";

    DienstplanObjektCloner cloner = DienstplanObjektCloner(DeployedAddresses.DienstplanObjektCloner());

    ///@notice Testet die Generierung und Editierung von Klonen des Typs Arbeit
    function testeErstellungVonArbeitsObjekten() public{
        DienstplanObjekt result = cloner.erzeugeArbeitsObjekt(1);

        Assert.equal(result.getMonat(), 1, "Should be the same.");
        Assert.equal(result.getTyp(), arbeitTest, "Should be the same");

        DienstplanObjekt result2 = cloner.erzeugeArbeitsObjekt(2);

        Assert.equal(result2.getMonat(), 2, "Should be the same.");
        Assert.equal(result2.getTyp(), arbeitTest, "Should be the same");

        result2.setMonat(3);
    
        Assert.equal(result.getMonat(), 1, "Should be the same.");
        Assert.equal(result.getTyp(), arbeitTest, "Should be the same");
        Assert.equal(result2.getMonat(), 3, "Should be the same.");
        Assert.equal(result2.getTyp(), arbeitTest, "Should be the same");
    }

    ///@notice Testet die Generierung und Editierung von Klonen des Typs Urlaub
    function testeErstellungVonUrlaubsObjekten() public{
        DienstplanObjekt result = cloner.erzeugeUrlaubsObjekt(1);

        Assert.equal(result.getMonat(), 1, "Should be the same.");
        Assert.equal(result.getTyp(), urlaubTest, "Should be the same");

        DienstplanObjekt result2 = cloner.erzeugeUrlaubsObjekt(2);

        Assert.equal(result2.getMonat(), 2, "Should be the same.");
        Assert.equal(result2.getTyp(), urlaubTest, "Should be the same");

        result2.setMonat(3);
    
        Assert.equal(result.getMonat(), 1, "Should be the same.");
        Assert.equal(result.getTyp(), urlaubTest, "Should be the same");
        Assert.equal(result2.getMonat(), 3, "Should be the same.");
        Assert.equal(result2.getTyp(), urlaubTest, "Should be the same");
    }

}