const Mp3Player = artifacts.require("Mp3Player");
const MusikSpielt = artifacts.require("MusikSpielt");
const MusikPausiert = artifacts.require("MusikPausiert");

module.exports = function (deployer) {
    deployer.deploy(Mp3Player);
    deployer.deploy(MusikSpielt);
    deployer.deploy(MusikPausiert);
};
