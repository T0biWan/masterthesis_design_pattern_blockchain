pragma solidity ^0.5.0;

import "./mp3player.sol";

///@title Interface,d as die Methoden der Zustände vorgibt
///@author Tobias Wamhof
interface Zustand {

    ///@notice Erste Version der Aktion, die bei Betätigen der Play/Pause Taste ausgeführt wird
    ///@param _mp3 MP3 Player Objekt
    function abspielen(Mp3Player _mp3) external;

    ///@notice Zweite Version der Aktion, die bei Betätigen der Play/Pause Taste ausgeführt wird
    ///@param _mp3 MP3 Player Objekt
    function abspielenNeu(Mp3Player _mp3) external;

    ///@notice Liefert die Beschreibung des Zustands
    ///@return Beschriebung des Zustands
    function beschreibung() external returns (string memory);

    ///@notice Setzt Zustand, der nach diesem gesetzt werden soll
    ///@param _zustand Folgezustand
    function setZustand(Zustand _zustand) external;
}
