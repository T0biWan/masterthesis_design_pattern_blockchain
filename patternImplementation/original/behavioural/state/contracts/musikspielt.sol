pragma solidity ^0.5.0;

import "./zustand.sol";

///@title Zustand, wenn Musik spielt
///@author Tobias Wamhof
contract MusikSpielt is Zustand {
    Zustand pausiert;

    ///@notice Erste Version der Aktion, die bei Betätigen der Play/Pause Taste ausgeführt wird
    ///@param _mp3 MP3 Player Objekt
    function abspielen(Mp3Player _mp3) external {
        _mp3.aendernZuPausiert();
    }

    ///@notice Zweite Version der Aktion, die bei Betätigen der Play/Pause Taste ausgeführt wird
    ///@param _mp3 MP3 Player Objekt
    function abspielenNeu(Mp3Player _mp3) external {
        _mp3.setZustand(pausiert);
    }

    ///@notice Liefert die Beschreibung des Zustands
    ///@return Beschriebung des Zustands
    function beschreibung() external returns (string memory) {
        return "Musik spielt";
    }

    ///@notice Setzt Zustand, der nach diesem gesetzt werden soll
    ///@param _zustand Folgezustand
    function setZustand(Zustand _zustand) external {
        pausiert = _zustand;
    }
}
