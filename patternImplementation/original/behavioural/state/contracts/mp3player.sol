pragma solidity ^0.5.0;

import "./musikpausiert.sol";
import "./musikspielt.sol";

///@title Repräsentiert einen MP3 Player
///@author Tobias Wamhof
contract Mp3Player {
    event PlayerEvent(string);
    Zustand aktuellerZustand = new MusikPausiert();

    ///@notice Erste Variante der Methode, die bei Betätigung der Play/Pause Taste aufgerufen werden könnte
    function abspielen() public {
        aktuellerZustand.abspielen(this);
    }

    ///@notice Zweite Variante der Methode, die bei Betätigung der Play/Pause Taste aufgerufen werden könnte
    function abspielenNeu() public {
        aktuellerZustand.abspielenNeu(this);
    }

    ///@notice Ändert aktuellen Zustand zu "Spielend"
    function aendernZuSpielend() public {
        aktuellerZustand = new MusikSpielt();
    }

    ///@notice Ändert aktuellen zustand zu "Pausiert"
    function aendernZuPausiert() public {
        aktuellerZustand = new MusikPausiert();
    }

    ///@notice Gibtd en aktuellen Zustand aus
    function ausgabeZustand() public {
        emit PlayerEvent(aktuellerZustand.beschreibung());
    }

    ///@notice Liefert den aktuellen Zustand
    ///@return aktueller Zustand
    function getZustand() public returns (string memory) {
        return aktuellerZustand.beschreibung();
    }

    ///@notice Setzt den aktuellen Zustand
    ///@param _neuerZustand neuer Zustand
    function setZustand(Zustand _neuerZustand) public {
        aktuellerZustand = _neuerZustand;
    }
}
