pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/mp3player.sol";

///@title Dient dem Testen des State Patterns
///@author Tobias Wamhof
contract StateTest {
    string testPlay = "Musik spielt";
    string testPause = "Musik pausiert";

    Mp3Player player = Mp3Player(DeployedAddresses.Mp3Player());
    Zustand pausiert = MusikPausiert(DeployedAddresses.MusikPausiert());
    Zustand spielend = MusikSpielt(DeployedAddresses.MusikSpielt());

    ///@notice Testet die erste Version des Änderns des Zustandes zu Spielend
    function testeAendernZuSpielend() public {
        player.aendernZuPausiert();

        Assert.equal(player.getZustand(), testPause, "Should be paused.");

        player.abspielen();

        Assert.equal(player.getZustand(), testPlay, "Should be playing.");
    }

    ///@notice Testet die erste Version des Änderns des Zustandes zu Pausiert
    function testeAendernZuPausiert() public {
        player.aendernZuSpielend();

        Assert.equal(player.getZustand(), testPlay, "Should be playing.");

        player.abspielen();

        Assert.equal(player.getZustand(), testPause, "Should be paused.");
    }

    ///@notice Testet die zweite Version des Änderns des Zustandes zu Spielend
    function testeAendernZuSpielendNeu() public {
        player.setZustand(pausiert);
        pausiert.setZustand(spielend);
        spielend.setZustand(pausiert);

        Assert.equal(player.getZustand(), testPause, "Should be paused.");

        player.abspielenNeu();

        Assert.equal(player.getZustand(), testPlay, "Should be playing.");
    }

    ///@notice Testet die zweite Version des Änderns des Zustandes zu Pausiert
    function testeAendernZuPausiertNeu() public {
        player.setZustand(spielend);
        pausiert.setZustand(spielend);
        spielend.setZustand(pausiert);
        
        Assert.equal(player.getZustand(), testPlay, "Should be playing.");

        player.abspielenNeu();

        Assert.equal(player.getZustand(), testPause, "Should be paused.");
    }
}
