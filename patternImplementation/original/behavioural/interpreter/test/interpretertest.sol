pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/additionausdruck.sol";
import "../contracts/subtraktionausdruck.sol";
import "../contracts/terminalausdruck.sol";

///@title Dient dem Testen des Interpreter Patterns
///@author Tobias Wamhof
///@notice Berechnet auf verschiedene Weisen die Rechnung 5 + 2 - 4 + 8
contract InterpreterTest {
   TerminalAusdruck fuenf = new TerminalAusdruck(5);
    TerminalAusdruck zwei = new TerminalAusdruck(2);
    TerminalAusdruck vier = new TerminalAusdruck(4);
    TerminalAusdruck vierNegativ = new TerminalAusdruck(-4);
    TerminalAusdruck acht = new TerminalAusdruck(8);

    Ausdruck ersterAusdruck;

    ///@notice Testet Variante 1
    function testeAusdruck1() public {
        AdditionAusdruck add1 = new AdditionAusdruck(fuenf, zwei);
        AdditionAusdruck add2 = new AdditionAusdruck(vierNegativ, acht);

        ersterAusdruck = new AdditionAusdruck(add1, add2);

        Assert.equal(ersterAusdruck.interpretiere(), 11, "Should be the same");
    }

    ///@notice Testet Variante 2
    function testeAusdruck2() public {
        AdditionAusdruck add = new AdditionAusdruck(fuenf, zwei);
        SubtraktionAusdruck sub = new SubtraktionAusdruck(add, vier);

        ersterAusdruck = new AdditionAusdruck(sub, acht);

        Assert.equal(ersterAusdruck.interpretiere(), 11, "Should be the same");
    }

    ///@notice Testet Variante 3
    function testeAusdruck3() public {
        SubtraktionAusdruck sub = new SubtraktionAusdruck(zwei, vier);
        AdditionAusdruck add = new AdditionAusdruck(fuenf, sub);

        ersterAusdruck = new AdditionAusdruck(add, acht);

        Assert.equal(ersterAusdruck.interpretiere(), 11, "Should be the same");
    }

    ///@notice Testet beispielhaft andere Rechnung
    function testeAusdruck4() public {
        SubtraktionAusdruck sub = new SubtraktionAusdruck(zwei, acht);
        AdditionAusdruck add1 = new AdditionAusdruck(sub, fuenf);

        ersterAusdruck = new AdditionAusdruck(add1, vier);

        Assert.equal(ersterAusdruck.interpretiere(), 3, "Should be the same");
    }
}
