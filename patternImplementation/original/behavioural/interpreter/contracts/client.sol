pragma solidity ^0.5.0;

import "./ausdruck.sol";
import "./subtraktionausdruck.sol";
import "./additionausdruck.sol";
import "./terminalausdruck.sol";

///@title Dient dem Testen des Interpreter Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(int256);

    TerminalAusdruck fuenf = new TerminalAusdruck(5);
    TerminalAusdruck zwei = new TerminalAusdruck(2);
    TerminalAusdruck vier = new TerminalAusdruck(4);
    TerminalAusdruck vierNegativ = new TerminalAusdruck(-4);
    TerminalAusdruck acht = new TerminalAusdruck(8);

    Ausdruck ersterAusdruck;

    ///@notice Testet die Rechnung 5 + 2 - 4 + 8
    ///@dev Variante 1
    function testeAusdruck1() public {
        AdditionAusdruck add1 = new AdditionAusdruck(fuenf, zwei);
        AdditionAusdruck add2 = new AdditionAusdruck(vierNegativ, acht);

        ersterAusdruck = new AdditionAusdruck(add1, add2);

        emit ClientEvent(ersterAusdruck.interpretiere());
    }

    ///@notice Testet die Rechnung 5 + 2 - 4 + 8    
    ///@dev Variante 2
    function testeAusdruck2() public {
        AdditionAusdruck add = new AdditionAusdruck(fuenf, zwei);
        SubtraktionAusdruck sub = new SubtraktionAusdruck(add, vier);

        ersterAusdruck = new AdditionAusdruck(sub, acht);

        emit ClientEvent(ersterAusdruck.interpretiere());
    }

    ///@notice Testet die Rechnung 5 + 2 - 4 + 8
    ///@dev Variante 3
    function testeAusdruck3() public {
        SubtraktionAusdruck sub = new SubtraktionAusdruck(zwei, vier);
        AdditionAusdruck add = new AdditionAusdruck(fuenf, sub);

        ersterAusdruck = new AdditionAusdruck(add, acht);

        emit ClientEvent(ersterAusdruck.interpretiere());
    }
}
