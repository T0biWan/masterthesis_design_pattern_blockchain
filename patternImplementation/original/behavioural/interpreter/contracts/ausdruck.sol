pragma solidity ^0.5.0;

///@title Repräsentiert einen Ausdruck der zu interpretierenden Syntax
///@author Tobias Wamhof
contract Ausdruck {
    function interpretiere() public returns (int256);
}
