pragma solidity ^0.5.0;

import "./berechnungsausdruck.sol";

///@title Repräsentiert eine Addition
///@author Tobias Wamhof
contract AdditionAusdruck is BerechnungsAusdruck {
    constructor(Ausdruck _ausdruck1, Ausdruck _ausdruck2)
        public
        BerechnungsAusdruck(_ausdruck1, _ausdruck2)
    {}

    ///@notice Interpretiert die beiden Ausdrücke und gibt deren Summe zurück.
    ///@return Summe der gespeicherten Ausdrücke.
    function interpretiere() public returns (int256) {
        return ausdruck1.interpretiere() + ausdruck2.interpretiere();
    }
}
