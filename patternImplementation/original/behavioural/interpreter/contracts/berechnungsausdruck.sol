pragma solidity ^0.5.0;

import "./ausdruck.sol";

///@title Oberklasse für Ausdrücke, die Berechnungen durchführen.
///@author Tobias Wamhof
contract BerechnungsAusdruck is Ausdruck {
    Ausdruck ausdruck1;
    Ausdruck ausdruck2;

    constructor(Ausdruck _ausdruck1, Ausdruck _ausdruck2) public {
        ausdruck1 = _ausdruck1;
        ausdruck2 = _ausdruck2;
    }

    ///@notice Soll die beiden gespeicherten Ausdrücke auswerten
    ///@dev Genaue Implementation geschieht in jeweiliger Unterklasse
    ///@return Berechnetes Ergebnis
    function interpretiere() public returns (int256) {}
}
