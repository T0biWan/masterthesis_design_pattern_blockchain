pragma solidity ^0.5.0;

import "./ausdruck.sol";

///@title Repräsentiert einen einfachen Integerwert für die Berechnungen
///@author Tobias Wamhof
contract TerminalAusdruck is Ausdruck {
    int256 data;

    constructor(int256 _data) public {
        data = _data;
    }

    ///@notice Implementierung der vorgegebenen interpretiere Methode.
    ///@return den gespeicherten Integerwert
    function interpretiere() public returns (int256) {
        return data;
    }
}
