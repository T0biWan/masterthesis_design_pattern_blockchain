const TerminalAusdruck = artifacts.require("TerminalAusdruck");
const AdditionAusdruck = artifacts.require("AdditionAusdruck");
const SubtraktionAusdruck = artifacts.require("SubtraktionAusdruck");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(TerminalAusdruck, 1).then(function () {
    deployer.deploy(Client);
    deployer.deploy(SubtraktionAusdruck, TerminalAusdruck.address, TerminalAusdruck.address);
    return deployer.deploy(AdditionAusdruck, TerminalAusdruck.address, TerminalAusdruck.address);
  });
};
