pragma solidity ^0.5.0;

import "./geldschein.sol";

contract Client {
    Geldschein hoechsterSchein;

    constructor(Geldschein _hoechsterWert) public {
        hoechsterSchein = _hoechsterWert;
    }

    function testeWert(uint256 _wert) public {
        hoechsterSchein.auszahlen(_wert);
    }
}
