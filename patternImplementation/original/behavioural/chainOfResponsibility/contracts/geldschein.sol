pragma solidity ^0.5.0;

///@title Oberklasse zur Repräsentation eines Gliedes der Kette
///@author Tobias Wamhof
contract Geldschein {
    Geldschein internal nextHandler;
    uint256 internal wert;

    constructor(Geldschein _nextHandler, uint256 _wert) public {
        nextHandler = _nextHandler;
        wert = _wert;
    }

    ///@notice Zahlt einen übergebenen Betrag aus und liefert als String, welcher Schein wie oft ausgezahlt werden kann. Pro Schein wird ein Event erzeugt
    ///@param _betrag Betrag der auszuzahlen ist
    ///@return String, welcher Schein wie oft ausgezahlt werden kann.
    function auszahlen(uint256 _betrag) public returns (string memory) {
        uint256 anzahl = _betrag / wert;
        uint256 restlicherBetrag = _betrag - (anzahl * wert);

        for (uint256 i = 0; i < anzahl; i++) {
            emitEvent();
        }

        if (!isLast() && restlicherBetrag > 0) {
            return
                strConcat(
                    uintToStr(anzahl),
                    "x",
                    uintToStr(wert),
                    "+",
                    nextHandler.auszahlen(restlicherBetrag)
                );
        } else {
            return strConcat(uintToStr(anzahl), "x", uintToStr(wert), "", "");
        }
    }

    function emitEvent() public;

    ///@notice Dient des Erkennens des letzten Gliedes der Kette
    ///@return true falls Ende der Kette erreicht ist,f alse wenn nicht
    function isLast() public view returns (bool) {
        return address(nextHandler) == address(0);
    }

    ///@notice Fügt 5 Strings aneinander
    ///@dev Code entnommen von https://ethereum.stackexchange.com/questions/729/how-to-concatenate-strings-in-solidity
    ///@param . Strings, die zusammengeführt werden sollen
    ///@return zusammengefügter String
    function strConcat(
        string memory _a,
        string memory _b,
        string memory _c,
        string memory _d,
        string memory _e
    ) internal pure returns (string memory) {
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);
        bytes memory _bc = bytes(_c);
        bytes memory _bd = bytes(_d);
        bytes memory _be = bytes(_e);
        string memory abcde =
            new string(
                _ba.length + _bb.length + _bc.length + _bd.length + _be.length
            );
        bytes memory babcde = bytes(abcde);
        uint256 k = 0;
        for (uint256 i = 0; i < _ba.length; i++) babcde[k++] = _ba[i];
        for (uint256 i = 0; i < _bb.length; i++) babcde[k++] = _bb[i];
        for (uint256 i = 0; i < _bc.length; i++) babcde[k++] = _bc[i];
        for (uint256 i = 0; i < _bd.length; i++) babcde[k++] = _bd[i];
        for (uint256 i = 0; i < _be.length; i++) babcde[k++] = _be[i];
        return string(babcde);
    }

    ///@notice Konvertiert einen Integerwert in einen String
    ///@dev Code entnommen aus https://stackoverflow.com/questions/47129173/how-to-convert-uint-to-string-in-solidity
    ///@param _i Integer der konvertiert werden soll
    ///@return konvertierten Integer als String
    function uintToStr(uint256 _i)
        internal
        pure
        returns (string memory _uintAsString)
    {
        uint256 number = _i;
        if (number == 0) {
            return "0";
        }
        uint256 j = number;
        uint256 len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint256 k = len - 1;
        while (number != 0) {
            bstr[k--] = bytes1(uint8(48 + (number % 10)));
            number /= 10;
        }
        return string(bstr);
    }
}
