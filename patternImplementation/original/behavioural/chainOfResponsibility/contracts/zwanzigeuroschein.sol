pragma solidity ^0.5.0;

import "./geldschein.sol";

///@title Repräsentiert den 20 Euro Schein als Glied der Kette.
///@author Tobias Wamhof
contract ZwanzigEuroSchein is Geldschein {
    event ZwanzigerEvent(string);

    constructor(Geldschein _nextHandler) public Geldschein(_nextHandler, 20){}

    function emitEvent() public {
        emit ZwanzigerEvent("Zahle 20er aus.");
    }
}
