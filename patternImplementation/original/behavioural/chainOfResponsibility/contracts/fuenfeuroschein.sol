pragma solidity ^0.5.0;

import "./geldschein.sol";

///@title Repräsentiert den 5 Euro Schein als Glied der Kette.
///@author Tobias Wamhof
contract FuenfEuroSchein is Geldschein {
    event FuenferEvent(string);

    constructor() public Geldschein(Geldschein(address(0)), 5){}

    function emitEvent() public {
        emit FuenferEvent("Zahle 5er aus.");
    }
}

