pragma solidity ^0.5.0;

import "./geldschein.sol";

///@title Repräsentiert den 50 Euro Schein als Glied der Kette.
///@author Tobias Wamhof
contract FuenfzigEuroSchein is Geldschein {
    event FuenfzigerEvent(string);

    constructor(Geldschein _nextHandler) public Geldschein(_nextHandler, 50){}

    function emitEvent() public {
        emit FuenfzigerEvent("Zahle 50er aus.");
    }
}
