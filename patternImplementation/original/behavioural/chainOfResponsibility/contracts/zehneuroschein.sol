pragma solidity ^0.5.0;

import "./geldschein.sol";

///@title Repräsentiert den 10 Euro Schein als Glied der Kette.
///@author Tobias Wamhof
contract ZehnEuroSchein is Geldschein {
    event ZehnerEvent(string);

    constructor(Geldschein _nextHandler) public Geldschein(_nextHandler, 10){}

    function emitEvent() public {
        emit ZehnerEvent("Zahle 10er aus.");
    }
}
