const LetzterSchein = artifacts.require("LetzterSchein");
const FuenfEuroSchein = artifacts.require("FuenfEuroSchein");
const ZehnEuroSchein = artifacts.require("ZehnEuroSchein");
const ZwanzigEuroSchein = artifacts.require("ZwanzigEuroSchein");
const FuenfzigEuroSchein = artifacts.require("FuenfzigEuroSchein");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(FuenfEuroSchein).then(function () {
    return deployer.deploy(ZehnEuroSchein, FuenfEuroSchein.address).then(function () {
      return deployer.deploy(ZwanzigEuroSchein, ZehnEuroSchein.address).then(function () {
        return deployer.deploy(FuenfzigEuroSchein, ZwanzigEuroSchein.address).then(function () {
          return deployer.deploy(Client, FuenfzigEuroSchein.address);
        });
      });
    });
  });
};
