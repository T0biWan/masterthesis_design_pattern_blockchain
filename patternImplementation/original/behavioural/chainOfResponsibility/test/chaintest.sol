pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/geldschein.sol";
import "../contracts/fuenfeuroschein.sol";
import "../contracts/zehneuroschein.sol";
import "../contracts/zwanzigeuroschein.sol";
import "../contracts/fuenfzigeuroschein.sol";

///@title Dient dem Testen der Contracts für das Chain of Responsibility Pattern
///@author Tobias Wamhof
contract ChainTest {
    Geldschein scheinFuenf = new FuenfEuroSchein();
    Geldschein scheinZehn = new ZehnEuroSchein(scheinFuenf);
    Geldschein scheinZwanzig = new ZwanzigEuroSchein(scheinZehn);
    Geldschein schein = new FuenfzigEuroSchein(scheinZwanzig);

    function testeBetrag85() public {
        Assert.equal(
            schein.auszahlen(85),
            "1x50+1x20+1x10+1x5",
            "Should be the same."
        );
    }

    function testeBetrag120() public {
        Assert.equal(
            schein.auszahlen(120),
            "2x50+1x20",
            "Should be the same."
        );
    }

    function testeBetrag45() public {
        Assert.equal(
            schein.auszahlen(45),
            "0x50+2x20+0x10+1x5",
            "Should be the same."
        );
    }

    function testeBetrag0() public {
        Assert.equal(
            schein.auszahlen(0),
            "0x50",
            "Should be the same."
        );
    }
}
