pragma solidity ^0.5.0;

import "./dataminer.sol";

///@title DataMiner zum Analysieren von Word Dateien
///@author Tobias Wamhof
contract WordDataMiner is DataMiner {
    event WordDataMinerEvent(string);

    ///@notice Funktionalität zum Öffnen einer Word Datei
    ///@return String zum Testen
    function oeffneDatei() public returns(string memory){
        emit WordDataMinerEvent("Word Funktionalität: Öffnen");
        return "Word Funktionalität: Öffnen";
    }

    ///@notice Funktionalität zum Extrahieren von Daten aus einer Word Datei
    ///@return String zum Testen
    function extrahiereDaten() public returns(string memory){
        emit WordDataMinerEvent("Word Funktionalität: Extrahiere Daten");
        return "Word Funktionalität: Extrahiere Daten";
    }

    ///@notice Funktionalität zum Schließen einer Word Datei
    ///@return String zum Testen
    function schliesseDatei() public returns(string memory){
        emit WordDataMinerEvent("Word Funktionalität: Schließen");
        return "Word Funktionalität: Schließen";
    }
}
