pragma solidity ^0.5.0;

import "./dataminer.sol";

///@title DataMiner zum Analysieren von PDF Dateien
///@author Tobias Wamhof
contract PdfDataMiner is DataMiner {
    event PdfDataMinerEvent(string);

    ///@notice Funktionalität zum Öffnen einer PDF Datei
    ///@return String zum Testen
    function oeffneDatei() public returns(string memory){
        emit PdfDataMinerEvent("PDF Funktionalität: Öffnen");
        return "PDF Funktionalität: Öffnen";
    }

    ///@notice Funktionalität zum Extrahieren von Daten aus einer PDF Datei
    ///@return String zum Testen
    function extrahiereDaten() public returns(string memory){
        emit PdfDataMinerEvent("PDF Funktionalität: Extrahiere Daten");
        return "PDF Funktionalität: Extrahiere Daten";
    }

    ///@notice Funktionalität zum Schließen einer PDF Datei
    ///@return String zum Testen
    function schliesseDatei() public returns(string memory){
        emit PdfDataMinerEvent("PDF Funktionalität: Schließen");
        return "PDF Funktionalität: Schließen";
    }
}
