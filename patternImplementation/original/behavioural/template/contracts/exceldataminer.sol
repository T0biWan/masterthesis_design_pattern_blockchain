pragma solidity ^0.5.0;

import "./dataminer.sol";

///@title DataMiner zum Analysieren von Excel Dateien
///@author Tobias Wamhof
contract ExcelDataMiner is DataMiner {
    event ExcelDataMinerEvent(string);

    ///@notice Funktionalität zum Öffnen einer Excel Datei
    ///@return String zum Testen
    function oeffneDatei() public returns(string memory) {
        emit ExcelDataMinerEvent("Excel Funktionalität: Öffnen");
        return "Excel Funktionalität: Öffnen";
    }

    ///@notice Funktionalität zum Extrahieren von Daten aus einer Excel Datei
    ///@return String zum Testen
    function extrahiereDaten() public returns(string memory){
        emit ExcelDataMinerEvent("Excel Funktionalität: Extrahiere Daten");
        return "Excel Funktionalität: Extrahiere Daten";
    }

    ///@notice Funktionalität zum Schließen einer Excel Datei
    ///@return String zum Testen
    function schliesseDatei() public returns(string memory){
        emit ExcelDataMinerEvent("Excel Funktionalität: Schließen");
        return "Excel Funktionalität: Schließen";
    }
}
