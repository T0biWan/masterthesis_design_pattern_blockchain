pragma solidity ^0.5.0;

import "./dataminer.sol";

///@title Dient dem Testen des Template Patterns
///@author Tobias Wamhof
contract Client {
    DataMiner aktuellerMiner;

    ///@notice Führt gewählte Version des DataMiners aus
    function ausfuehren() public {
        aktuellerMiner.analysiereDatei();
        aktuellerMiner.oeffneDatei();
        aktuellerMiner.extrahiereDaten();
        aktuellerMiner.analysiereDaten();
        aktuellerMiner.schliesseDatei();
    }

    ///@notice Setzt gewählte Implementation des DataMiners
    function setAktuellerDataMiner(DataMiner _neuerMiner) public {
        aktuellerMiner = _neuerMiner;
    }
}
