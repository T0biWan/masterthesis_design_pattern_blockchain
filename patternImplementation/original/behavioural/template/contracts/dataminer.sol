pragma solidity ^0.5.0;

///@title Template für verschiedenje Arten von DataMinern
///@author Tobias Wamhof
contract DataMiner {
    event DataMinerEvent(string);

    ///@notice Gemeinsame Funktionalität zum Analysieren eines Dateipfades
    ///@return String zum Testen
    function analysiereDatei() public returns (string memory){
        emit DataMinerEvent("GemeinsameFunktionalität: Pfad prüfen");
        return "GemeinsameFunktionalität: Pfad prüfen";
    }

    ///@notice Abstrakte Funktionalität zum Öffnen einer Datei
    ///@return String zum Testen
    function oeffneDatei() public returns (string memory);

    ///@notice Abstrakte Funktionalität zum Extrahieren von Daten
    ///@return String zum Testen
    function extrahiereDaten() public returns (string memory);

    ///@notice Gemeinsame Funktionalität zum Analysieren einer Datei
    ///@return String zum Testen
    function analysiereDaten() public returns (string memory){
        emit DataMinerEvent("GemeinsameFunktionalität: Daten analysieren");
        return "GemeinsameFunktionalität: Daten analysieren";
    }

    ///@notice Abstrakte Funktionalität zum Schließen einer Datei
    ///@return String zum Testen
    function schliesseDatei() public returns (string memory);
}
