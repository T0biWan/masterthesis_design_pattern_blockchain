pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/dataminer.sol";
import "../contracts/exceldataminer.sol";
import "../contracts/pdfdataminer.sol";
import "../contracts/worddataminer.sol";

///@title Dient dem Testen des Template Patterns
///@author Tobias Wamhof
contract TemplateTest {
    DataMiner miner;

    ///@notice Testet den ExcelDataMiner
    function testeExcelMiner() public {
        miner = ExcelDataMiner(DeployedAddresses.ExcelDataMiner());

        Assert.equal(
            miner.analysiereDatei(),
            "GemeinsameFunktionalität: Pfad prüfen",
            "Should be the same."
        );
        Assert.equal(
            miner.oeffneDatei(),
            "Excel Funktionalität: Öffnen",
            "Should be the same."
        );
        Assert.equal(
            miner.extrahiereDaten(),
            "Excel Funktionalität: Extrahiere Daten",
            "Should be the same."
        );
        Assert.equal(
            miner.analysiereDaten(),
            "GemeinsameFunktionalität: Daten analysieren",
            "Should be the same."
        );
        Assert.equal(
            miner.schliesseDatei(),
            "Excel Funktionalität: Schließen",
            "Should be the same."
        );
    }

    ///@notice Testet den PDFDataMiner
    function testePDFMiner() public {
        miner = PdfDataMiner(DeployedAddresses.PdfDataMiner());

        Assert.equal(
            miner.analysiereDatei(),
            "GemeinsameFunktionalität: Pfad prüfen",
            "Should be the same."
        );
        Assert.equal(
            miner.oeffneDatei(),
            "PDF Funktionalität: Öffnen",
            "Should be the same."
        );
        Assert.equal(
            miner.extrahiereDaten(),
            "PDF Funktionalität: Extrahiere Daten",
            "Should be the same."
        );
        Assert.equal(
            miner.analysiereDaten(),
            "GemeinsameFunktionalität: Daten analysieren",
            "Should be the same."
        );
        Assert.equal(
            miner.schliesseDatei(),
            "PDF Funktionalität: Schließen",
            "Should be the same."
        );
    }

    ///@notice Testet den WordDataMiner
    function testeWordMiner() public {
        miner = WordDataMiner(DeployedAddresses.WordDataMiner());

        Assert.equal(
            miner.analysiereDatei(),
            "GemeinsameFunktionalität: Pfad prüfen",
            "Should be the same."
        );
        Assert.equal(
            miner.oeffneDatei(),
            "Word Funktionalität: Öffnen",
            "Should be the same."
        );
        Assert.equal(
            miner.extrahiereDaten(),
            "Word Funktionalität: Extrahiere Daten",
            "Should be the same."
        );
        Assert.equal(
            miner.analysiereDaten(),
            "GemeinsameFunktionalität: Daten analysieren",
            "Should be the same."
        );
        Assert.equal(
            miner.schliesseDatei(),
            "Word Funktionalität: Schließen",
            "Should be the same."
        );
    }
}
