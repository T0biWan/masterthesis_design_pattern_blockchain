const ExcelDataMiner = artifacts.require("ExcelDataMiner");
const PdfDataMiner = artifacts.require("PdfDataMiner");
const WordDataMiner = artifacts.require("WordDataMiner");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Client);
  deployer.deploy(WordDataMiner);
  deployer.deploy(PdfDataMiner);
  deployer.deploy(ExcelDataMiner);
};