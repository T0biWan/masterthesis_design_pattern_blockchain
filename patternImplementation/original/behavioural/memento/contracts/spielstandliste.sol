pragma solidity ^0.5.0;

import "./spielstand.sol";

///@title Speichert alle Spielstände in einer Liste
///@author Tobias Wamhof
contract SpielstandListe {
    Spielstand[] spielstaende;

    ///@notice Fügt der Liste einen Spielstand hinzu
    ///@param _stand hinzuzufügender Spielstand
    function spielstandHinzufuegen(Spielstand _stand) public {
        spielstaende.push(_stand);
    }

    ///@notice Entfernt einen Spielstand aus der Liste
    ///@param _index Index des zu entfernenden Spielstands
    function spielstandEntfernen(uint _index) public {
        require(spielstaende.length > _index && _index >= 0);
        spielstaende[_index] = spielstaende[spielstaende.length - 1];
        delete spielstaende[spielstaende.length - 1];
        spielstaende.length--;
    }

    ///@notice Lädt Spielstand aus der Liste
    ///@param _index Index des zu ladenden Spielstands
    ///@return angefragter Spielstand
    function spielstandLaden(uint256 _index) public view returns (Spielstand) {
        require(_index >= 0 && _index < spielstaende.length);
        return spielstaende[_index];
    }

    ///@notice Liefert Anzahl gespeicherter Spielstände
    ///@return Anzahl der Spielstände
    function getSpielstaendeLength() public view returns (uint256) {
        return spielstaende.length;
    }
}
