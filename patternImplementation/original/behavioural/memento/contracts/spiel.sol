pragma solidity ^0.5.0;

import "./spielstand.sol";

///@title Repräsentiert ein Spiel
///@author Tobias Wamhof
contract Spiel {
    uint256 level = 0;

    ///@notice Speichert aktuelles Level in neuem Spielstand
    ///@return erzeugter Spielstand
    function spielstandSpeichern() public returns (Spielstand) {
        return new Spielstand(level);
    }

    ///@notice lädt das Level aus einem Spielstand
    ///@param _spielstand zu ladender Spielstand
    function spielstandLaden(Spielstand _spielstand) public {
        level = _spielstand.getLevel();
    }

    ///@notice Erhäht Level des Spiels um 1
    function naechstesLevel() public {
        level++;
    }

    ///@notice Liefert das aktuelle Level des Spiels
    ///@return aktuelles Level
    function getLevel() public view returns (uint256) {
        return level;
    }
}
