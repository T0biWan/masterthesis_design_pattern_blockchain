pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/verlag.sol";
import "../contracts/fazverlag.sol";
import "../contracts/zeitung.sol";
import "../contracts/abonnent.sol";
import "../contracts/abonnentpdf.sol";
import "../contracts/abonnentprint.sol";

///@title Dient dem Testen des Observer Patterns
///@author Tobias Wamhof
contract ObserverTest {
    Verlag verlag = FAZVerlag(DeployedAddresses.FAZVerlag());
    Abonnent print = AbonnentPrint(DeployedAddresses.AbonnentPrint());

    ///@notice Testet verteilen von leerer Zeitung
    function testeVerteilenVonNullZeitung() public {
       verlag.setZeitung(Zeitung(address(0)));

        bytes memory payload =
            abi.encodeWithSignature("verteileZeitung()", "Tobias");
        (bool success, bytes memory response) = address(verlag).call(payload);

        Assert.equal(success, false, "Should not work.");
    }

    ///@notice Testet hinzufüegen und entfernen von Abonnenten
    function testeHinzufuegenUndEntfernenVonAbonnent() public {
       Assert.equal(verlag.getAnzahlAbos(), 2, "Should be initial filled.");

       verlag.aboHinzufuegen(print);

       Assert.equal(verlag.getAnzahlAbos(), 3, "Should have increased.");

       verlag.aboEntfernen(print);

       Assert.equal(verlag.getAnzahlAbos(), 2, "Should be initial filled.");
    }

    ///@notice Testet verteilen von Zeitung
    function testeVerteilenVonZeitung() public {
       verlag.setZeitung(new Zeitung("test"));

       verlag.verteileZeitung();

       Assert.equal(print.getAktuelleZeitung().getTitel(), "test", "should have been delivered.");
    }
}
