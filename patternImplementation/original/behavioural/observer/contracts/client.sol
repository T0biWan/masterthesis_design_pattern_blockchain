pragma solidity ^0.5.0;

import "./verlag.sol";
import "./abonnent.sol";
import "./zeitung.sol";

///@title Dient dem Testen des Observer Patterns
///@author Tobias Wamhof
contract Client{
    Verlag verlag;
    Abonnent pdfAbo;
    Abonnent printAbo;

    constructor(Verlag _verlag, Abonnent _pdfAbo, Abonnent _printAbo) public{
        verlag = _verlag;
        pdfAbo = _pdfAbo;
        printAbo = _printAbo;
    }

    ///@notice Verteilt die aktuelle zeitung an alle Abonnenten
    function verteileZeitung() public{
        verlag.verteileZeitung();
    }

    ///@notice Erstellt eine neue Zeitung
    ///@param _titel Titel der neuen Zeitung
    function erstelleNeueZeitung(string memory _titel) public {
        verlag.setZeitung(new Zeitung(_titel));
    }

    ///@notice Entfernt den PDF Abonennten
    function entfernePDFAbo() public{
        verlag.aboEntfernen(pdfAbo);
    }

    ///@notice Entfernt den Print Abonnenten
    function entfernePrintAbo() public{
        verlag.aboEntfernen(printAbo);
    }

    ///@notice Fügt einen PDF Abonnenten hinzu
    function hinzufuegenPdfAbo()public{
        verlag.aboHinzufuegen(pdfAbo);
    }

    ///@notice Fügt einen print Abonnenten hinzu
    function hinzufuegenPrintAbo() public{
        verlag.aboHinzufuegen(printAbo);
    }
}