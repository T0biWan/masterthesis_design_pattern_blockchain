pragma solidity ^0.5.0;

import "./verlag.sol";

///@title Repräsentiert die konkrete Implementation eines Verlags
///@author Tobias Wamhof
contract FAZVerlag is Verlag {

    ///@notice fügt ein Abonnenten hinzu
    ///@param _abonnent hinzuzufügender Abonnent
    function aboHinzufuegen(Abonnent _abonnent) public {
        abonnenten.push(_abonnent);
    }

    ///@notice Entfernt einen Abonnenten
    ///@param _abonnent zu entfernender Abonnent
    function aboEntfernen(Abonnent _abonnent) public {
        for (uint256 i; i < abonnenten.length; i++) {
            if (abonnenten[i] == _abonnent) {
                abonnenten[i] = abonnenten[abonnenten.length - 1];
                delete abonnenten[abonnenten.length-1];
                abonnenten.length--;
                return;
            }
        }
    }

    ///@notice Verteilt die aktuelle Zeitung an alle Abonnenten
    function verteileZeitung() public {
        require(address(aktuelleZeitung) != address(0));
        for (uint256 i; i < abonnenten.length; i++) {
            abonnenten[i].erhalteZeitung(aktuelleZeitung);
        }
    }

    ///@notice Weißt die aktuellste Zeitung neu zu
    ///@param _neueZeitung neue Version der Zeitung
    function setZeitung(Zeitung _neueZeitung) public {
        aktuelleZeitung = _neueZeitung;
    }
}
