pragma solidity ^0.5.0;

import "./abonnent.sol";
import "./zeitung.sol";
import "./verlag.sol";

///@title Repräsentiert einen Abonennten der PDF Version einer Zeitung
///@author Tobias Wamhof
contract AbonnentPDF is Abonnent {
    event PDFEvent(string);

    constructor(string memory _name, Verlag _verlag)
        public
        Abonnent(_name, _verlag)
    {}

    ///@notice Weist dem Abonennten eines neue Zeitung zu
    ///@param _zeitung neue Zeitung
    function erhalteZeitung(Zeitung _zeitung) public {
        aktuelleZeitung = _zeitung;
        emit PDFEvent(
            string(
                abi.encodePacked(name, " erhält Zeitung ", _zeitung.getTitel())
            )
        );
    }
}
