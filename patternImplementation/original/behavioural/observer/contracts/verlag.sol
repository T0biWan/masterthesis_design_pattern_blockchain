pragma solidity ^0.5.0;

import "./abonnent.sol";
import "./zeitung.sol";

///@title Repräsentiert die einen abstrakten Verlag
///@author Tobias Wamhof
contract Verlag {
    Abonnent[] abonnenten;

    Zeitung aktuelleZeitung;

    ///@notice fügt ein Abonnenten hinzu
    ///@param _abonnent hinzuzufügender Abonnent
    function aboHinzufuegen(Abonnent _abonnent) public;

    ///@notice Entfernt einen Abonnenten
    ///@param _abonnent zu entfernender Abonnent
    function aboEntfernen(Abonnent _abonnent) public;

    ///@notice Verteilt die aktuelle Zeitung an alle Abonnenten
    function verteileZeitung() public;

    ///@notice Weißt die aktuellste Zeitung neu zu
    ///@param _neueZeitung neue Version der Zeitung
    function setZeitung(Zeitung _neueZeitung) public;

    ///@notice liefert aktuelle Anzahl der Abonnenten
    ///@return aktuelle Anzahl der Abonennten
    function getAnzahlAbos() public view returns (uint){
        return abonnenten.length;
    }
}
