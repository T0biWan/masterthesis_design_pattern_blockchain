const Zeitung = artifacts.require("Zeitung");
const FAZVerlag = artifacts.require("FAZVerlag");
const AbonnentPDF = artifacts.require("AbonnentPDF");
const AbonnentPrint = artifacts.require("AbonnentPrint");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Zeitung, "Template");
  deployer.deploy(FAZVerlag).then(function(){
    deployer.deploy(AbonnentPDF, "pdfAbo", FAZVerlag.address);
    return deployer.deploy(AbonnentPrint, "printAbo", FAZVerlag.address).then(function(){
      return deployer.deploy(Client, FAZVerlag.address, AbonnentPDF.address, AbonnentPrint.address);
    });
  });
};
