pragma solidity ^0.5.0;

import "./hardwarevisitor.sol";
import "./hardware.sol";

///@title Konkrete Implementierung eines HardwareVisitors, der verschiedene Hardware Komponenten besuchen kann
///@author Tobias Wamhof
contract HardwareAnalyseVisitor is HardwareVisitor{

    Hardware[] hardware;

    ///@notice Besucht der Reihe nach alle hinzugefügten Komponenten
    function besucheHardwareKomponenten() external{
        for(uint i = 0; i < hardware.length; i++){
            hardware[i].akzeptiereVisitor(this);
        }
    }

    ///@notice Fügt den zu besuchenden Komponenten eine Komponente hinzu
    ///@param _hardware zu besuchende Komponente
    function hardwareHinzufuegen(Hardware _hardware) public {
        hardware.push(_hardware);
    }

    ///@notice Leert der Liste der zu besuchenden Komponenten
    function hardwareEntfernen() public {
        delete hardware;
        hardware.length = 0;
    }

}