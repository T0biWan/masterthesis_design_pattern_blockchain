pragma solidity ^0.5.0;

import "./hardware.sol";

///@title Repräsentiert einen Prozessor als besuchbaren Contract
///@author Tobias Wamhof
contract Prozessor is Hardware{

    uint private anzahlBesucher = 0;

    ///@notice Nimmt einen Besucher entgegen
    ///@param _visitor der Besucher
    function akzeptiereVisitor(HardwareVisitor _visitor) external{
        anzahlBesucher++;
    }

    ///@notice Liefert Anzahl von Besuchen
    ///@return Anzahl von Besuchen
    function getAnzahl() public view returns (uint){
        return anzahlBesucher;
    }

}