pragma solidity ^0.5.0;

import "./hardwarevisitor.sol";

///@title Interface für Hardwarekomponenten, die einen Besucher entgegennehmen können müssen
///@author Tobias Wamhof
interface Hardware{

    ///@notice Nimmt einen Besucher entgegen
    ///@param _visitor der Besucher
    function akzeptiereVisitor(HardwareVisitor _visitor) external;

}