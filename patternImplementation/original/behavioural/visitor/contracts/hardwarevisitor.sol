pragma solidity ^0.5.0;

///@title Interface das Besuchern Methodensignatur zum besuchen vorgibt
///@author Tobias Wamhof
interface HardwareVisitor{

    ///@notice Besucht der Reihe nach alle hinzugefügten Komponenten
    function besucheHardwareKomponenten()external;

}