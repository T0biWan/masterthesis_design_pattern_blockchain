const Prozessor = artifacts.require("Prozessor");
const Grafikkarte = artifacts.require("Grafikkarte");
const HardwareAnalyseVisitor = artifacts.require("HardwareAnalyseVisitor");

module.exports = function (deployer) {
  deployer.deploy(Prozessor);
  deployer.deploy(Grafikkarte);
  deployer.deploy(HardwareAnalyseVisitor);
};
