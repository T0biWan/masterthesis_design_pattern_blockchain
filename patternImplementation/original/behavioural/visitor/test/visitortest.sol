pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/prozessor.sol";
import "../contracts/grafikkarte.sol";
import "../contracts/hardwareanalysevisitor.sol";

///@title Dient dem Testen des Visitor Patterns
///@author Tobias Wamhof
contract VisitorTest{
    Prozessor prozessor = Prozessor(DeployedAddresses.Prozessor());
    Grafikkarte grafikkarte = Grafikkarte(DeployedAddresses.Grafikkarte());
    HardwareAnalyseVisitor visitor = HardwareAnalyseVisitor(DeployedAddresses.HardwareAnalyseVisitor());

    ///@notice Testet das Besuchen von Hardwarekomponenten
    function testeBesuch() public{
        visitor.hardwareHinzufuegen(prozessor);
        visitor.hardwareHinzufuegen(grafikkarte);

        visitor.besucheHardwareKomponenten();

        Assert.equal(prozessor.getAnzahl(), 1, "Should be 1.");
        Assert.equal(grafikkarte.getAnzahl(), 1, "Should be 1.");
    }

}