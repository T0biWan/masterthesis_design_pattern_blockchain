pragma solidity ^0.5.0;

import "./chatteilnehmer.sol";

///@title Repräsentiert einen Chat Bot
///@author Tobias Wamhof
contract Bot is ChatTeilnehmer {
    event BotEvent(string, string);

    constructor(ChatRoom _mediator, string memory _name) public ChatTeilnehmer(_name){
        mediator = _mediator;
        mediator.botHinzufuegen(this);
    }

    ///@notice Sendet eine Nachricht mit dem Bot als Absender
    ///@param _nachricht zu sendende Nachricht
    function sendeNachricht(string memory _nachricht) public {
        mediator.senden(_nachricht, name, false);
    }

    ///@notice Empfängt eine Nachricht
    ///@param _nachricht empfangene Nachricht
    ///@param _absender Sender der Nachricht
    function empfangeNachricht(string memory _nachricht, string memory _absender)
        public
    {
        aktuellsteNachricht = _nachricht;
        emit BotEvent(_nachricht, _absender);
    }
}
