const List = artifacts.require("List");
const LinkedList = artifacts.require("LinkedList");
const ListIterator = artifacts.require("ListIterator");
const LinkedListIterator = artifacts.require("LinkedListIterator");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(List);
  deployer.deploy(LinkedList);
  deployer.deploy(ListIterator);
  deployer.deploy(LinkedListIterator);
  deployer.deploy(Client);
};
