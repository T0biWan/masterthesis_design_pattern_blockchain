pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/list.sol";
import "../contracts/linkedlist.sol";
import "../contracts/collection.sol";

///@title Dient dem Testen des Iterator Patterns
///@author Tobias Wamhof
contract LinkedListIteratorTest {

    ///@notice testet die Linked List
    function testLinkedList() public {
        uint[] memory returned = new uint[](3);
        LinkedList collection = LinkedList(DeployedAddresses.LinkedList());
        collection.addObject(1);
        collection.addObject(2);
        collection.addObject(3);

        Iterator it = collection.createIterator();

        uint counter = 0;
        while (it.hasNext()) {
            returned[counter] = it.next().getWert();
            counter++;
        }
        Assert.equal(returned[0], 3, "First Entry should be 3.");
        Assert.equal(returned[1], 2, "Second Entry should be 2.");
        Assert.equal(returned[2], 1, "Third Entry should be 1.");
        Assert.equal(counter, 3, "Should iterate over 3 entries.");
    }

    ///@notice testet die Liste
    function testList() public {
        uint[] memory returned = new uint[](3);
        List collection = List(DeployedAddresses.List());
        collection.addObject(1);
        collection.addObject(2);
        collection.addObject(3);

        Iterator it = collection.createIterator();

        uint counter = 0;
        while (it.hasNext()) {
            returned[counter] = it.next().getWert();
            counter++;
        }
        Assert.equal(returned[0], 1, "First Entry should be 3.");
        Assert.equal(returned[1], 2, "Second Entry should be 2.");
        Assert.equal(returned[2], 3, "Third Entry should be 1.");
        Assert.equal(counter, 3, "Should iterate over 3 entries.");
    }
}
