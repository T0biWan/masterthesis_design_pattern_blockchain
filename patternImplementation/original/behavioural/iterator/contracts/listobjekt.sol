pragma solidity ^0.5.0;

import "./objekt.sol";

///@title Objekt, das in einer List verwaltet wird.
///@author Tobias Wamhof
contract ListObjekt is Objekt {
    constructor(uint256 _wert) public Objekt(_wert) {}
}
