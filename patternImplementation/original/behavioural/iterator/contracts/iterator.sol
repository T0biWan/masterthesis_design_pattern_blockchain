pragma solidity ^0.5.0;

import "./objekt.sol";

///@title Repräsentiert das Interface für Iteratoren 
///@author Tobias Wamhof
interface Iterator {

    ///@notice Liefert das nächste Element einer Liste zurück, die vom Iterator durchschritten wird
    ///@return nächstes Objekt
    function next() external returns (Objekt);

    ///@notice Überprüft, ob am Ende der zu iterierenden Liste angekommen wurde
    ///@return true, falls noch Elemente existieren, false wenn nicht
    function hasNext() external returns (bool);
}
