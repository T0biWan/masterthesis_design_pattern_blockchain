pragma solidity ^0.5.0;

///@title Repräsentiert ein einfaches Objekt, das einen Wert beinhaltet.
///@author Tobias Wamhof
contract Objekt {
    uint256 wert;

    constructor(uint256 _wert) public {
        wert = _wert;
    }

    function getWert() public view returns (uint256) {
        return wert;
    }
}
