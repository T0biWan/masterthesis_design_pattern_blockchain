pragma solidity ^0.5.0;

import "./list.sol";
import "./iterator.sol";

///@title Dient der Itearion über eine normale Liste
///@author Tobias Wamhof
contract ListIterator is Iterator {
    uint256 currentIndex;
    List list;

    constructor() public {
        currentIndex = 0;
    }

    ///@notice Liefert das nächste Objekt der Liste
    ///@return nächstes Objekt
    function next() external returns (Objekt) {
        return list.getValues()[currentIndex++];
    }

    ///@notice Überprüft, ob alle Objekte durchschritten wurden
    ///@return true, falls noch Objekte übrig sind, false wenn nicht
    function hasNext() external returns (bool) {
        return currentIndex + 1 <= list.getValues().length;
    }

    ///@notice Setzt zu betrachtende Liste
    ///@param _list zu iterierende Liste
    function setList(List _list) public {
        list = _list;
    }
}
