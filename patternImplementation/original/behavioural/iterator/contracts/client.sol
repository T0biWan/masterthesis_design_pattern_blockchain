pragma solidity ^0.5.0;

import "./collection.sol";
import "./list.sol";
import "./linkedlist.sol";
import "./iterator.sol";

///@title Dient dem Testen des Iterator Patterns
///@author Tobias Wamhof
contract Client {
    Collection collection;
    event ClientEvent(uint256);

    ///@notice Testet das Iterieren über eine Liste
    function testList() public {
        collection = new List();
        ausfuehren();
    }

    ///@notice Testet das Iterieren über eine Linked List
    function testLinkedList() public {
        collection = new LinkedList();
        ausfuehren();
    }

    ///@notice Fügt einer Liste Objekte hinzu und iteriert über diese
    function ausfuehren() private {
        collection.addObject(1);
        collection.addObject(2);
        collection.addObject(3);

        Iterator it = collection.createIterator();
        while (it.hasNext()) {
            emit ClientEvent(it.next().getWert());
        }
    }
}
