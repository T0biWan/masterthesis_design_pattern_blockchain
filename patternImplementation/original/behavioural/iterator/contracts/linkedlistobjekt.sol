pragma solidity ^0.5.0;

import "./objekt.sol";

///@title Objekt, das in einer LinkedList verwaltet wird.
///@author Tobias Wamhof
contract LinkedListObjekt is Objekt {
    LinkedListObjekt next;
    LinkedListObjekt previous;

    constructor(uint256 _wert) public Objekt(_wert) {}

    function setNext(address _next) public {
        next = LinkedListObjekt(_next);
    }

    function setPrevious(address _previous) public {
        previous = LinkedListObjekt(_previous);
    }

    function getNext() public view returns (LinkedListObjekt) {
        return next;
    }

    function getPrevious() public view returns (LinkedListObjekt) {
        return previous;
    }
}
