pragma solidity ^0.5.0;

import "./iterator.sol";
import "./linkedlistobjekt.sol";
import "./linkedlistiterator.sol";
import "./collection.sol";

///@title Repräsentiert eine Implementation einer verketteten Liste
///@author Tobias Wamhof
contract LinkedList is Collection {
    LinkedListObjekt firstItem;

    ///@notice Erzeugt einen Iterator für eine verkettete Liste
    ///@return Iterator Objekt
    function createIterator() external returns (Iterator) {
        LinkedListIterator it = new LinkedListIterator();
        it.setObjekt(firstItem);
        return it;
    }

    ///@notice Fügt der verketteten Liste am Anfang ein Objekt hinzu
    ///@param _wert Wert der hinzugefügt werden soll
    function addObject(uint256 _wert) public {
        LinkedListObjekt objekt = new LinkedListObjekt(_wert);
        if (address(firstItem) == address(0)) {
            objekt.setNext(address(0));
        } else {
            objekt.setNext(address(firstItem));
            firstItem.setPrevious(address(objekt));
        }
        objekt.setPrevious(address(0));
        firstItem = objekt;
    }

    ///@notice Liefert das erste Element der Liste
    ///@return erstes Element
    function getFirst() public view returns (LinkedListObjekt){
        return firstItem;
    }
}
