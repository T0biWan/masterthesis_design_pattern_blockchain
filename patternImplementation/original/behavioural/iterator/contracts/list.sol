pragma solidity ^0.5.0;

import "./listobjekt.sol";
import "./iterator.sol";
import "./listiterator.sol";
import "./collection.sol";

///@title Repräsentiert eine Implementation einer normalen Liste in Form eines Arrays
///@author Tobias Wamhof
contract List is Collection{

    ListObjekt[] items;

    ///@notice Erzeugt einen Iterator für eine Liste
    ///@return Iterator Objekt
    function createIterator() external returns (Iterator){
        ListIterator it = new ListIterator();
        it.setList(this);
        return it;
    }

    ///@notice Liefert alle Werte der Liste
    ///@return die Objekte
    function getValues() public view returns (ListObjekt[] memory){
        return items;
    }

    ///@notice Fügt der Liste ein Objekt hinzu
    ///@param _wert hinzuzufügendes Objekt
    function addObject(uint _wert) public {
        ListObjekt object = new ListObjekt(_wert);
        items.push(object);
    }
    
}