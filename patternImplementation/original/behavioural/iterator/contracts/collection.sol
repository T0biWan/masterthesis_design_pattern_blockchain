pragma solidity ^0.5.0;

import "./objekt.sol";
import "./iterator.sol";

///@title Repräsentiert das Interface für verschiedene Collections
///@author Tobias Wamhof
interface Collection {
    
    ///@notice Erzeugt einen Iterator
    ///@return Iterator Objekt
    function createIterator() external returns (Iterator);

    ///@notice Fügt der Liste ein Element hinzu
    ///@param _wert Hinzuzufügender Wert
    function addObject(uint256 _wert) external;
}
