pragma solidity ^0.5.0;

import "./linkedlistobjekt.sol";
import "./iterator.sol";

///@title Dient der Itearion über eine verkettete Liste
///@author Tobias Wamhof
contract LinkedListIterator is Iterator{

    LinkedListObjekt currentObject;

    ///@notice Liefert das nächste Objekt der Liste
    ///@return nächstes Objekt
    function next() external returns (Objekt){
        LinkedListObjekt temp = currentObject;
        currentObject = currentObject.getNext();
        return temp;
    }

    ///@notice Überprüft, ob alle Objekte durchschritten wurden
    ///@return true, falls noch Objekte übrig sind, false wenn nicht
    function hasNext() external returns (bool){
        return (address(currentObject) != address(0));
    }

    ///@notice Setzt aktuell zu betrachtendes Objekt
    ///@param _objekt aktuelles Objekt
    function setObjekt(LinkedListObjekt _objekt) public {
        currentObject = _objekt;
    }
    
}