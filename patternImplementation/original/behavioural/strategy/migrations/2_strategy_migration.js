const Addition = artifacts.require("Addition");
const Subtraktion = artifacts.require("Subtraktion");
const Multiplikation = artifacts.require("Multiplikation");
const Taschenrechner = artifacts.require("Taschenrechner");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
    deployer.deploy(Addition);
    deployer.deploy(Subtraktion);
    deployer.deploy(Multiplikation);
    deployer.deploy(Taschenrechner).then(function () {
        return deployer.deploy(Client, Taschenrechner.address);
    });
};