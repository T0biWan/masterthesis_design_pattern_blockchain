pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/taschenrechner.sol";
import "../contracts/addition.sol";
import "../contracts/subtraktion.sol";
import "../contracts/multiplikation.sol";

///@title Dient dem Testen des Strategy Patterns
///@author Tobias Wamhof
contract StrategyTest {
    Taschenrechner rechner = Taschenrechner(DeployedAddresses.Taschenrechner());
    Addition add = Addition(DeployedAddresses.Addition());
    Subtraktion sub = Subtraktion(DeployedAddresses.Subtraktion());
    Multiplikation mul = Multiplikation(DeployedAddresses.Multiplikation());

    ///@notice Testet die Additionsstrategie
    function testeAddition() public {
        rechner.setAktuelleFunktion(add);

        Assert.equal(rechner.berechnungAusfuehren(5, 3), 8, "Should be the same.");
    }

    ///@notice Testet die Subtraktionsstrategie
    function testeSubtraktion() public {
        rechner.setAktuelleFunktion(sub);

        Assert.equal(rechner.berechnungAusfuehren(5, 3), 2, "Should be the same.");
    }

    ///@notice Testet die Multiplikationsstrategie
    function testeMultiplikation() public {
        rechner.setAktuelleFunktion(mul);

        Assert.equal(rechner.berechnungAusfuehren(5, 3), 15, "Should be the same.");
    }
}
