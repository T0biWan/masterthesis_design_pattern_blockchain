pragma solidity ^0.5.0;

import "./operation.sol";

///@title Repräsentiert die Multiplikation als Strategie zum Umgang mit 2 Integerwerten
///@author Tobias Wamhof
contract Multiplikation is Operation {

    ///@notice Berechnet das Produkt aus 2 Integer Werten
    ///@param _wert1 Faktor 1
    ///@param _wert2 Faktor 2
    ///@return Produkt der Parameter
    function berechne(int256 _wert1, int256 _wert2) external returns (int256) {
        return _wert1 * _wert2;
    }
}
