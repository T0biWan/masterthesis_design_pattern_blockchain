pragma solidity ^0.5.0;

import "./operation.sol";

///@title Repräsentiert die Subtraktion als Strategie zum Umgang mit 2 Integerwerten
///@author Tobias Wamhof
contract Subtraktion is Operation {

    ///@notice Berechnet die Differenz aus 2 Integer Werten
    ///@param _wert1 Minuend
    ///@param _wert2 Subtrahend
    ///@return Differenz der Parameter
    function berechne(int256 _wert1, int256 _wert2) external returns (int256) {
        return _wert1 - _wert2;
    }
}
