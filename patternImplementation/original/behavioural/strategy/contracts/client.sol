pragma solidity ^0.5.0;

import "./taschenrechner.sol";
import "./addition.sol";
import "./subtraktion.sol";
import "./multiplikation.sol";

///@title Dient dem Testen des Strategy Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(int256);
    Taschenrechner taschenrechner;

    constructor(Taschenrechner _tr) public {
        taschenrechner = _tr;
    }

    ///@notice Setzt Addition als zu nutzende Strategie
    function nutzeAddition() public{
        taschenrechner.setAktuelleFunktion(new Addition());
    }

    ///@notice Setzt Subtraktion als zu nutzende Strategie
    function nutzeSubtraktion() public{
        taschenrechner.setAktuelleFunktion(new Subtraktion());
    }

    ///@notice Setzt Multiplikation als zu nutzende Strategie
    function nutzeMultiplition() public{
        taschenrechner.setAktuelleFunktion(new Multiplikation());
    }

    ///@notice Führt aktuelle Operation mit 2 Integerwerten aus
    ///@param _w1 erster Wert
    ///@param _w2 zweiter Wert
    function testeAktuelleOperation(int256 _w1, int256 _w2) public {
        emit ClientEvent(taschenrechner.berechnungAusfuehren(_w1, _w2));
    }
}
