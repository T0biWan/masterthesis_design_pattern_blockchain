pragma solidity ^0.5.0;

import "./operation.sol";

///@title Repräsentiert die Addition als Strategie zum Umgang mit 2 Integerwerten
///@author Tobias Wamhof
contract Addition is Operation {

    ///@notice Berechnet die Summe aus 2 Integer Werten
    ///@param _wert1 Summand 1
    ///@param _wert2 Summand 2
    ///@return Summe der Parameter
    function berechne(int256 _wert1, int256 _wert2) external returns (int256) {
        return _wert1 + _wert2;
    }
}
