pragma solidity ^0.5.0;

///@title Interface das Schnittstelle für Operation beschreibt
///@author Tobias Wamhof
interface Operation {

    ///@notice Wendet Strategie auf 2 Integerwerte an.
    ///@param _wert1 erster Integer
    ///@param _wert2 zweiter Integer
    ///@return Ergebnis
    function berechne(int256 _wert1, int256 _wert2) external returns (int256);
}
