pragma solidity ^0.5.0;

import "./operation.sol";

///@title Ermöglicht Austausch einer Strategie zum Umgang mit 2 Integerwerten
///@author Tobias Wamhof
contract Taschenrechner {
    Operation aktuelleOperation;

    ///@notice führt Berechnung mit gewählter Strategie aus
    ///@param _wert1 erster Integer
    ///@param _wert2 zweiter Integer
    ///@return Ergebnis
    function berechnungAusfuehren(int256 _wert1, int256 _wert2) public returns (int256) {
        return aktuelleOperation.berechne(_wert1, _wert2);
    }

    ///@notice Ermöglicht Auswahl der zu nutzenden Strategie
    ///@param _neueOperation gewählte Strategie
    function setAktuelleFunktion(Operation _neueOperation) public {
        aktuelleOperation = _neueOperation;
    }
}
