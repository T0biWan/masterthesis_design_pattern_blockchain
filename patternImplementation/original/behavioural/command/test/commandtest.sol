pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/haendler.sol";
import "../contracts/kaufauftrag.sol";
import "../contracts/verkaufauftrag.sol";
import "../contracts/aktie.sol";

///@title Dient dem Testen des Command Pattern
///@author Tobias Wamhof
contract CommandTest {
   Haendler haendler = Haendler(DeployedAddresses.Haendler());
   Aktie testaktie;

   ///@notice Testet das Ausführen eines Kaufauftrages.
   function testeKaufauftrag() public {
      testaktie = new Aktie("Test", 10);
      KaufAuftrag kauf = new KaufAuftrag(testaktie);

      haendler.auftragHinzufuegen(kauf);

      Assert.equal(haendler.anzahlAuftraege(), 1, "Should be the same.");

      haendler.auftraegeAusfuehren();

      Assert.equal(haendler.anzahlAuftraege(), 0, "Should be the same.");
      Assert.equal(testaktie.getMenge(), 11, "Should be the same.");
   }

   ///@notice Testet das Ausführen eines Verkaufauftrages.
   function testeVerkaufauftrag() public {
      testaktie = new Aktie("Test", 10);
      VerkaufAuftrag verkauf = new VerkaufAuftrag(testaktie);

      haendler.auftragHinzufuegen(verkauf);

      Assert.equal(haendler.anzahlAuftraege(), 1, "Should be the same.");

      haendler.auftraegeAusfuehren();

      Assert.equal(haendler.anzahlAuftraege(), 0, "Should be the same.");
      Assert.equal(testaktie.getMenge(), 9, "Should be the same.");
   }

   ///@notice Testet das Ausführen zweier Kaufaufträge.
   function teste2Kaufauftraege() public {
      testaktie = new Aktie("Test", 10);
      KaufAuftrag kauf = new KaufAuftrag(testaktie);
      KaufAuftrag kauf2 = new KaufAuftrag(testaktie);

      haendler.auftragHinzufuegen(kauf);
      haendler.auftragHinzufuegen(kauf2);

      Assert.equal(haendler.anzahlAuftraege(), 2, "Should be the same.");

      haendler.auftraegeAusfuehren();

      Assert.equal(haendler.anzahlAuftraege(), 0, "Should be the same.");
      Assert.equal(testaktie.getMenge(), 12, "Should be the same.");
   }

   ///@notice Testet das Ausführen zweier Verkaufaufträge.
   function teste2Verkaufauftraege() public {
      testaktie = new Aktie("Test", 10);
      VerkaufAuftrag verkauf = new VerkaufAuftrag(testaktie);
      VerkaufAuftrag verkauf2 = new VerkaufAuftrag(testaktie);

      haendler.auftragHinzufuegen(verkauf);
      haendler.auftragHinzufuegen(verkauf2);

      Assert.equal(haendler.anzahlAuftraege(), 2, "Should be the same.");

      haendler.auftraegeAusfuehren();

      Assert.equal(haendler.anzahlAuftraege(), 0, "Should be the same.");
      Assert.equal(testaktie.getMenge(), 8, "Should be the same.");
   }
}
