const Aktie = artifacts.require("Aktie");
const KaufAuftrag = artifacts.require("KaufAuftrag");
const VerkaufAuftrag = artifacts.require("VerkaufAuftrag");
const Haendler = artifacts.require("Haendler");
const Client = artifacts.require("Client");

module.exports = function (deployer) {
  deployer.deploy(Aktie, "Test", 0).then(function () {
    deployer.deploy(KaufAuftrag, Aktie.address);
    deployer.deploy(Haendler);
    return deployer.deploy(VerkaufAuftrag, Aktie.address).then(function () {
      return deployer.deploy(Client, Haendler.address, KaufAuftrag.address, VerkaufAuftrag.address);
    })

  });
};
