pragma solidity ^0.5.0;

///@title Repräsentiert eine Aktie
///@author Tobias Wamhof
contract Aktie {
    string private name;
    uint private menge;

    constructor(string memory _name, uint _menge) public {
        name = _name;
        menge = _menge;
    }

    ///@notice Erhöht die Menge der gekauften Aktien um 1.
    function kaufe() public {
        menge++;
    }

    ///@notice Verringert die Menge der gekauften Aktien um 1.
    function verkaufe() public {
        menge--;
    }

    ///@notice Liefert die MEnge der gekauften Aktien
    ///@return Menge gekaufter Aktien
    function getMenge() public view returns(uint){
        return menge;
    }
}
