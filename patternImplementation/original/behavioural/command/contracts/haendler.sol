pragma solidity ^0.5.0;

import "./auftrag.sol";

///@title Repräsentiert den Händler, der Aufträge ausführen kann.
///@author Tobias Wamhof
contract Haendler {
    Auftrag[] auftraege;

    ///@notice Fügt einen Auftrag zur Liste der Aufträge hinzu.
    ///@param _auftrag Auftrag, der hinzugefügt werden soll.
    function auftragHinzufuegen(Auftrag _auftrag) public {
        auftraege.push(_auftrag);
    }

    ///@notice Führt alle gespeicherten Aufträge aus.
    function auftraegeAusfuehren() public {
        for (uint256 i = 0; i < auftraege.length; i++) {
            auftraege[i].ausfuehren();
        }
        delete auftraege;
    }

    ///@notice Liefert die Anzahl der gespeicherten Aufträge
    ///@return Anzahl der Aufträge
    function anzahlAuftraege() public view returns (uint){
        return auftraege.length;
    }
}
