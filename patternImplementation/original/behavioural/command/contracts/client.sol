pragma solidity ^0.5.0;

import "./aktie.sol";
import "./auftrag.sol";
import "./haendler.sol";
import "./kaufauftrag.sol";
import "./verkaufauftrag.sol";

///@title Dient dem Testen des Command Patterns
///@author Tobias Wamhof
contract Client {
    Haendler haendler;
    KaufAuftrag kauf;
    VerkaufAuftrag verkauf;

    constructor(
        Haendler _haendler,
        KaufAuftrag _kauf,
        VerkaufAuftrag _verkauf
    ) public {
        haendler = _haendler;
        kauf = _kauf;
        verkauf = _verkauf;
    }

    ///@notice Testet den Verkauf einer Aktie.
    function testeVerkauf() public {
        haendler.auftragHinzufuegen(verkauf);
        haendler.auftraegeAusfuehren();
    }

    ///@notice Testet den Kauf einer Aktie.
    function testeKauf() public {
        haendler.auftragHinzufuegen(kauf);
        haendler.auftraegeAusfuehren();
    }
}
