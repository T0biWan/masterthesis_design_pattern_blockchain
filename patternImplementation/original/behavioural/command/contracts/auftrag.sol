pragma solidity ^0.5.0;

import "./aktie.sol";

///@title Repräsentiert einen Auftrag.
///@author Tobias Wamhof
///@notice abstrakte Oberklasse für KaufAuftrag und VerkaufAuftrag
contract Auftrag {
    Aktie aktie;

    constructor(Aktie _aktie) public {
        aktie = _aktie;
    }

    function ausfuehren() public;
}
