pragma solidity ^0.5.0;

import "./auftrag.sol";

///@title Repräsentiert einen Auftrag zum Verkaufen einer Aktie.
///@author Tobias Wamhof
contract VerkaufAuftrag is Auftrag {
    event VerkaufAuftragEvent();

    constructor(Aktie _aktie) public Auftrag(_aktie) {}

    ///@notice Verkauft eine der im Konstruktor übergebenen Aktie.
    function ausfuehren() public {
        aktie.verkaufe();
        emit VerkaufAuftragEvent();
    }
}
