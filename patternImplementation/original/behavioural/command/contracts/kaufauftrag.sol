pragma solidity ^0.5.0;

import "./auftrag.sol";

///@title Repräsentiert einen Auftrag zum Kaufen einer Aktie.
///@author Tobias Wamhof
contract KaufAuftrag is Auftrag {
    event KaufAuftragEvent();

    constructor(Aktie _aktie) public Auftrag(_aktie) {}

    ///@notice Kauft eine der im Konstruktor übergebenen Aktie.
    function ausfuehren() public {
        aktie.kaufe();
        emit KaufAuftragEvent();
    }
}
