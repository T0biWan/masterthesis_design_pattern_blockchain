pragma solidity ^0.5.0;

import "./kundenkartei.sol";

///@title Dient dem Testen des Null Object Patterns
///@author Tobias Wamhof
contract Client {
    event ClientEvent(string);

    KundenKartei kundenKartei = new KundenKartei();

    ///@notice Testet das Anfragen eines Namens
    ///@param _name angefragter Name
    function testeName(string memory _name) public {
        emit ClientEvent(kundenKartei.getKunde(_name).getName());
    }

    ///@notice Fügt einen Kunden hinzu
    ///@param _name Name des Kunden, der hinzugefügt werden soll
    function hinzufuegen(string memory _name) public {
        kundenKartei.hinzufuegen(_name);
    }

    ///@notice Entfernt einen Kunden
    ///@param _name Name des Kunden, der entfernt werden soll
    function entferne(string memory _name) public {
        kundenKartei.loeschen(_name);
    }
}
