pragma solidity ^0.5.0;

import "./kunde.sol";
import "./echterkunde.sol";
import "./nullkunde.sol";

///@title Speichert Kunden nach ihren Namen in einem Mapping
///@author Tobias Wamhof
contract KundenKartei {
    mapping(string => Kunde) kunden;

    ///@notice Sucht im Mapping nach einem Kunden
    ///@param _name Name des Kunden
    ///@return Ein Kudnenobjekt
    function getKunde(string memory _name) public returns (Kunde) {
        if (address(kunden[_name]) != address(0)) {
            return kunden[_name];
        } else {
            return new NullKunde(_name);
        }
    }

    ///@notice Fügt dem Mapping einen Kunden hinzu
    ///@param _name Name des Kunden
    function hinzufuegen(string memory _name) public {
        kunden[_name] = new EchterKunde(_name);
    }

    ///@notice Entfernt einen Kunden aus dem Mapping
    ///@param _name Name des Kunden
    function loeschen(string memory _name) public{
        delete kunden[_name];
    }
}
