pragma solidity ^0.5.0;

import "./kunde.sol";

///@title Repräsentiert einen Kunden, der nicht gefunden werden konnte
///@author Tobias Wamhof
contract NullKunde is Kunde {
    constructor(string memory _name) public Kunde(string(abi.encodePacked(_name," konnte nicht gefunden werden."))){}
}
