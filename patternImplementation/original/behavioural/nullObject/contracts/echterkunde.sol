pragma solidity ^0.5.0;

import "./kunde.sol";

///@title Repräsentiert einen realen Kunden
///@author Tobias Wamhof
contract EchterKunde is Kunde {
    constructor(string memory _name) public Kunde(_name){}
}
